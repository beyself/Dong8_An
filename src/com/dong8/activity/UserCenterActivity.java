package com.dong8.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.dong8.R;
import com.dong8.resp.RespUser;
import com.dong8.util.PreferencesUtils;
import com.dong8.util.ToastUtil;
import com.dong8.util.Utils;

public class UserCenterActivity extends Activity implements OnClickListener 
{
	TextView tvAbout,my_favi;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_center);

		findViewById(R.id.info).setOnClickListener(this);
		findViewById(R.id.order).setOnClickListener(this);
		findViewById(R.id.member).setOnClickListener(this);
		findViewById(R.id.pay_log).setOnClickListener(this);
		
//		tvVersion =(TextView)findViewById(R.id.tvVersion);
//		tvVersion.setText("当前版本  "+Utils.getVersionName(UserCenterActivity.this));
//		tvVersion.setOnClickListener(this);
		tvAbout =(TextView)findViewById(R.id.tvAbout);
		tvAbout.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				showDialog("  关于动吧","掌上场馆预定App \n佛盛龙公司版权所有\n当前版本: " 
				+ Utils.getVersionName(UserCenterActivity.this));
			}
		});
		
		my_favi=(TextView)findViewById(R.id.my_favi);
		
		my_favi.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(UserCenterActivity.this, MyFavoriteActivity.class));
			}
		});
		
		findViewById(R.id.logout).setOnClickListener(this);
	}
	
	
	@Override
	protected void onResume() 
	{
		super.onResume();
	}

	@Override
	public void onClick(View arg0) 
	{
		RespUser.User user = Utils.getUserInfo(UserCenterActivity.this);
		if(user==null)
		{
			startActivity(new Intent(this, LoginActivity.class));
			return;
		}
		
		switch (arg0.getId()) 
		{
		case R.id.info:
			startActivity(new Intent(this, MemberInfoActivity.class));
			break;

		case R.id.order:
			startActivity(new Intent(this, MyReserveActivity.class));
			break;

		case R.id.pay_log:
			startActivity(new Intent(this, PaymentLogActivity.class));
			break;

		case R.id.member:
			startActivity(new Intent(this, MemberCardActivity.class));
			break;

		case R.id.my_favi:
			startActivity(new Intent(this, MyFavoriteActivity.class));
			break;

		case R.id.logout:
			showQueryDialog();
			break;
		}
	}
	
	public void showDialog(String title, String text) 
	{
		final Dialog dialog = new Dialog(this,R.style.dialog);
		dialog.setContentView(R.layout.dialog); 
		dialog.setCancelable(true);
		
		((Button)dialog.findViewById(R.id.confirm)).setText("我知道了");
		dialog.findViewById(R.id.confirm).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				dialog.dismiss();
			}
		});
		dialog.findViewById(R.id.cancel).setVisibility(View.GONE);
		((TextView)dialog.findViewById(R.id.msg)).setText(text);
		dialog.show();
	}
	
	public void showQueryDialog() 
	{
		final Dialog dialog = new Dialog(this,R.style.dialog);
		dialog.setContentView(R.layout.dialog); 
		dialog.setCancelable(true);
		
		((Button)dialog.findViewById(R.id.confirm)).setText("注销帐号");
		dialog.findViewById(R.id.confirm).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				PreferencesUtils.putString(UserCenterActivity.this, "user", "");
				ToastUtil.showToastWithOkPic("您已成功注销该帐号");
				
				dialog.dismiss();
			}
		});
		dialog.findViewById(R.id.cancel).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				dialog.dismiss();
			}
		});
		((TextView)dialog.findViewById(R.id.msg)).setText("是否注销该帐号？");
		dialog.show();
	}
	
	@Override
	public void onBackPressed() {
		this.getParent().onBackPressed();
	}
}
