package com.dong8.activity;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.dong8.R;
import com.dong8.adapter.ReserveAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class PaymentLogActivity extends Activity 
{
	ReserveAdapter m_adapter;
	ArrayList<JSONObject> m_array_list = new ArrayList<JSONObject>();
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_payment_log);
		
		((TextView)findViewById(R.id.tv_title)).setText("我的消费记录");
		findViewById(R.id.ibBack).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0) 
			{
				finish();
			}
		});

		final PullToRefreshListView ptrlvHeadlineNews = (PullToRefreshListView) findViewById(R.id.listview);
		ptrlvHeadlineNews.setMode(Mode.PULL_UP_TO_REFRESH);
		ptrlvHeadlineNews.setOnRefreshListener(new OnRefreshListener<ListView>() 
		{
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) 
			{
			}
		});

		ListView listview = ptrlvHeadlineNews.getRefreshableView();
		listview.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) 
			{
/*					String url = m_array_list.get(arg2 - 1).getString("Link");
					Intent intent = new Intent(RecommendGymActivity.this,WebviewActivity.class);
					intent.putExtra("url", url);
					startActivity(intent);*/
			}
		});
		
		m_adapter = new ReserveAdapter(PaymentLogActivity.this,null);
		listview.setAdapter(m_adapter);
	}

	class MyAdapter extends BaseAdapter
	{
		public int getCount() 
		{
//			return m_array_list.size();
			return 10;
		}

		public Object getItem(int position) 
		{
			return null;
		}

		public long getItemId(int position) 
		{
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) 
		{
			ViewHolder holder = null;
			if (convertView == null) 
			{
				holder = new ViewHolder();

				LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.item_pay_log, null);
				
				holder.title = (TextView)convertView.findViewById(R.id.title);
				
				convertView.setTag(holder);
			} 
			else 
			{
				holder = (ViewHolder) convertView.getTag();
			}
			
			holder.title.setText("贺龙体育馆\n4号羽毛球场");
			return convertView;
		}
	}
	
	public final class ViewHolder 
	{
		public TextView title;
		public TextView price;
		public TextView time;
		
		public int pos;
	}
}
