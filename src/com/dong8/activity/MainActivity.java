package com.dong8.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.Toast;

import com.dong8.R;
import com.dong8.sys.MyApp;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity implements OnCheckedChangeListener {
	private long exitTime = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maintabs);
		
		UmengUpdateAgent.setUpdateOnlyWifi(false);
		UmengUpdateAgent.update(this);
		UmengUpdateAgent.setDeltaUpdate(false);

		((RadioButton) findViewById(R.id.radio_button0)).setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.radio_button1)).setOnCheckedChangeListener(this);
		((RadioButton) findViewById(R.id.radio_button2)).setOnCheckedChangeListener(this);

		((RadioButton) findViewById(R.id.radio_button0)).setTextColor(Color.WHITE);
		setupIntent();
		/*
		 * Dialog dialog = new Dialog(this,R.style.dialog);
		 * dialog.setContentView(R.layout.dialog); dialog.setCancelable(true);
		 * dialog.show();
		 */
		
		((MyApp)getApplicationContext()).mFinishActivity = this;
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.radio_button0:
				this.getTabHost().setCurrentTabByTag("A_TAB");

				((RadioButton) findViewById(R.id.radio_button0)).setTextColor(Color.WHITE);
				((RadioButton) findViewById(R.id.radio_button1)).setTextColor(Color.rgb(114, 124, 167));
				((RadioButton) findViewById(R.id.radio_button2)).setTextColor(Color.rgb(114, 124, 167));
				break;
			case R.id.radio_button1:
				this.getTabHost().setCurrentTabByTag("B_TAB");

				((RadioButton) findViewById(R.id.radio_button0)).setTextColor(Color.rgb(114, 124, 167));
				((RadioButton) findViewById(R.id.radio_button1)).setTextColor(Color.WHITE);
				((RadioButton) findViewById(R.id.radio_button2)).setTextColor(Color.rgb(114, 124, 167));
				break;
			case R.id.radio_button2:
				this.getTabHost().setCurrentTabByTag("C_TAB");

				((RadioButton) findViewById(R.id.radio_button0)).setTextColor(Color.rgb(114, 124, 167));
				((RadioButton) findViewById(R.id.radio_button1)).setTextColor(Color.rgb(114, 124, 167));
				((RadioButton) findViewById(R.id.radio_button2)).setTextColor(Color.WHITE);
				break;
			}
		}
	}

	private void setupIntent() {
		getTabHost().addTab(
				buildTabSpec("A_TAB", R.string.main_page, R.drawable.selector_tab_recommend, new Intent(this,
						GymDetailHeadActivity.class)));

		getTabHost().addTab(
				buildTabSpec("B_TAB", R.string.faverite, R.drawable.selector_tab_favi, new Intent(this,
						FieldActivity.class)));

		getTabHost().addTab(
				buildTabSpec("C_TAB", R.string.my, R.drawable.selector_tab_my, new Intent(this,
						UserCenterActivity.class)));
	}

	private TabHost.TabSpec buildTabSpec(String tag, int resLabel, int resIcon, final Intent content) {
		return getTabHost().newTabSpec(tag).setIndicator(getString(resLabel), getResources().getDrawable(resIcon))
				.setContent(content);
	}

	@Override
	public void onBackPressed() {
		// Called by children
		exitApp();
	}

	public void exitApp() {
		if ((System.currentTimeMillis() - exitTime) > 2000) {
			Toast.makeText(this, "再按一次退出 动吧", Toast.LENGTH_LONG).show();
			exitTime = System.currentTimeMillis();
		} else {
			this.finish();
		}

	}

}
