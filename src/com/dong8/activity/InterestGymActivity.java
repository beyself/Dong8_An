package com.dong8.activity;

import java.util.Collections;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.resp.RespGym;
import com.dong8.resp.RespGymType;
import com.dong8.sys.Constants;
import com.dong8.type.Gym;
import com.dong8.type.GymType;
import com.dong8.util.ImageLoaderHelper;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.ToastUtil;
import com.loopj.android.http.RequestParams;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

public class InterestGymActivity extends Activity implements OnTouchListener {
	MyAdapter m_adapter;
	TypeAdapter m_TypeAdapter;
	boolean m_is_show = false;

	List<GymType> typeList = null;
	List<Gym> gymList = null;

	String movetype = "";
	int mSelected = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest_gym);

		
		findViewById(R.id.titleRight).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(InterestGymActivity.this, MyCityListActivity.class));
			}
		});
		
		
		ListView listview = (ListView) this.findViewById(R.id.listview);
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (!m_is_show) {
					m_is_show = true;
					findViewById(R.id.gym_layout).setAnimation(
							AnimationUtils.loadAnimation(InterestGymActivity.this, R.anim.push_left_out));
					findViewById(R.id.gym_layout).setVisibility(View.VISIBLE);
				}

				mSelected = arg2;
				m_TypeAdapter.notifyDataSetChanged();
				
				movetype = typeList.get(arg2).id;
				getGym(true);
			}
		});
		
		m_TypeAdapter = new TypeAdapter();
		listview.setAdapter(m_TypeAdapter);

		listview = (ListView) findViewById(R.id.listview_0);
/*		ptrlvHeadlineNews.setMode(Mode.PULL_UP_TO_REFRESH);
		ptrlvHeadlineNews.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			}
		});*/

		listview.setOnTouchListener(this);

		m_adapter = new MyAdapter();
		listview.setAdapter(m_adapter);

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Intent intent = new Intent(InterestGymActivity.this, GymDetailHeadActivity.class);
				Bundle bundle = new Bundle();
				bundle.putSerializable("gym", gymList.get(arg2 - 1));
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});

		getData(true);
	}

	class TypeAdapter extends BaseAdapter {
		public int getCount() {
			if (typeList == null)
				return 0;
			return typeList.size();
		}

		public Object getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();

				LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.item_sport_type, null);

				holder.img = (ImageView) convertView.findViewById(R.id.img);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.distance = (TextView) convertView.findViewById(R.id.distance);
//				holder.tvNum = (TextView) convertView.findViewById(R.id.tvNum);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			GymType type = typeList.get(position);

			holder.title.setText(type.typename);
			holder.distance.setText(" " + type.count + "家");
	/*		if (type.num > 0) {
				holder.tvNum.setText(type.num + "家在线预定");
			}*/

			// {"id":"tennis","typeName":"网球"}

			if (type.id.equals(Constants.TY_archery)) {
				holder.img.setBackgroundResource(R.drawable.ic_archery);
			} else if (type.id.equals(Constants.TY_badminton)) {
				holder.img.setBackgroundResource(R.drawable.ic_badminton);
			} else if (type.id.equals(Constants.TY_basketball)) {
				holder.img.setBackgroundResource(R.drawable.ic_basketball);
			} else if (type.id.equals(Constants.TY_bicycle)) {
				holder.img.setBackgroundResource(R.drawable.ic_bicycle);
			} else if (type.id.equals(Constants.TY_billiards)) {
				holder.img.setBackgroundResource(R.drawable.ic_billiards);
			} else if (type.id.equals(Constants.TY_bowling)) {
				holder.img.setBackgroundResource(R.drawable.s8);
			} else if (type.id.equals(Constants.TY_car)) {
				holder.img.setBackgroundResource(R.drawable.ic_car);
			} else if (type.id.equals(Constants.TY_darts)) {
				holder.img.setBackgroundResource(R.drawable.ic_darts);
			} else if (type.id.equals(Constants.TY_equestrian)) {
				holder.img.setBackgroundResource(R.drawable.ic_equestrian);
			} else if (type.id.equals(Constants.TY_football)) {
				holder.img.setBackgroundResource(R.drawable.ic_football);
			} else if (type.id.equals(Constants.TY_golf)) {
				holder.img.setBackgroundResource(R.drawable.ic_golf);
			} else if (type.id.equals(Constants.TY_kart)) {// -----------有问题
				holder.img.setBackgroundResource(R.drawable.ic_car);
			} else if (type.id.equals(Constants.TY_roller)) {
				holder.img.setBackgroundResource(R.drawable.ic_roller);
			} else if (type.id.equals(Constants.TY_shooting)) {
				holder.img.setBackgroundResource(R.drawable.ic_shooting);
			} else if (type.id.equals(Constants.TY_skating)) {
				holder.img.setBackgroundResource(R.drawable.ic_skating);
			} else if (type.id.equals(Constants.TY_ski)) {
				holder.img.setBackgroundResource(R.drawable.ic_ski);
			} else if (type.id.equals(Constants.TY_squash)) {// -----------有问题
				holder.img.setBackgroundResource(R.drawable.s8);
			} else if (type.id.equals(Constants.TY_swimming)) {
				holder.img.setBackgroundResource(R.drawable.ic_swimming);
			} else if (type.id.equals(Constants.TY_tableTennis)) {
				holder.img.setBackgroundResource(R.drawable.ic_tabletennis);
			} else if (type.id.equals(Constants.TY_tennis)) {
				holder.img.setBackgroundResource(R.drawable.ic_tennis);
			}

			if(mSelected == position)
			{
				convertView.setBackgroundColor(Color.rgb(234, 99, 51));
				holder.title.setTextColor(Color.WHITE);
			}
			else
			{
				convertView.setBackgroundColor(Color.WHITE);
				holder.title.setTextColor(Color.rgb(109, 109, 109));
			}
			
			return convertView;
		}
	}

	public final class ViewHolder {
		public TextView title;
		public TextView distance;
		public TextView tvNum;
		public ImageView img;
	}

	class MyAdapter extends BaseAdapter {
		public int getCount() {
			if (gymList == null)
				return 0;
			return gymList.size() + 1;
		}

		public Object getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (position == 0) {
				convertView = mInflater.inflate(R.layout.item_gym_filter, null);
			} else {
				convertView = mInflater.inflate(R.layout.item_gym, null);
				Gym gym = gymList.get(position - 1);
//				Gym gym = gymList.get(position );

				((TextView) convertView.findViewById(R.id.title)).setText(gym.clubName);
				((TextView) convertView.findViewById(R.id.distance)).setText(gym.price);
				((TextView) convertView.findViewById(R.id.price)).setText(gym.price);

				ImageLoaderHelper.displayImage(R.drawable.default_img_small, (ImageView) convertView.findViewById(R.id.img), gym.img);
			}

			return convertView;
		}
	}

	private float xPos = 0;
	private float yPos = 0;

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		if (MotionEvent.ACTION_DOWN == arg1.getAction()) {
			xPos = arg1.getX();
			yPos = arg1.getY();
		} else if (arg1.getX() - xPos >= 120 && Math.abs(yPos - arg1.getY()) < 100) {
			m_is_show = false;
			findViewById(R.id.gym_layout).setAnimation(
					AnimationUtils.loadAnimation(InterestGymActivity.this, R.anim.push_right_out));
			findViewById(R.id.gym_layout).setVisibility(View.GONE);

			mSelected = -1;
			m_TypeAdapter.notifyDataSetChanged();
		}

		return false;
	}

	private void getData(boolean show) {
		MgqDataHandler loginHandler = new MgqDataHandler(this, show, false) 
		{
			@SuppressWarnings("unchecked")
			@Override
			public void onSuccess(String response) {
				super.onSuccess(response);
				RespGymType ruser = JSON.parseObject(response, RespGymType.class);
				if (ruser.code.equals("0")) {
					typeList = ruser.data;
/*
					for (int i = 0; i < typeList.size(); i++) {
						GymType temp = (GymType) typeList.get(i);

						if (temp.id.equals("badminton")) {
							temp.sort = 1;
							temp.num = 3;
						}

						if (temp.id.equals("tableTennis")) {
							temp.sort = 2;
							temp.num = 3;
						}

					}*/
					Collections.sort(typeList);
					m_TypeAdapter.notifyDataSetChanged();
				} else {

					ToastUtil.showToastWithAlertPic(ruser.mesg);

				}
			}

			public void onFailure(Throwable ble) {
			}
		};

		RequestParams params = new RequestParams();
		MgqRestClient.get(Constants.CLUB_TYPE_COUNT, params, loginHandler);
	}

	private void getGym(boolean show) {
		MgqDataHandler loginHandler = new MgqDataHandler(this, show, false) {
			@Override
			public void onSuccess(String response) {
				super.onSuccess(response);
				RespGym ruser = JSON.parseObject(response, RespGym.class);
				if (ruser.code.equals("0")) {
					gymList = ruser.data;
					m_adapter.notifyDataSetChanged();
				} else {
					gymList = null;
					m_adapter.notifyDataSetChanged();
				}
			}

			public void onFailure(Throwable ble) {
			}
		};

		RequestParams params = new RequestParams();
		params.put("movetype", movetype);
		MgqRestClient.get(Constants.CLUB_TYPE_GYM, params, loginHandler);
	}

	@Override
	public void onBackPressed() {
		this.getParent().onBackPressed();
	}
}
