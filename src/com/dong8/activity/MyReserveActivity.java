package com.dong8.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.adapter.ReserveAdapter;
import com.dong8.resp.RespOrder;
import com.dong8.resp.RespOrder.OrderForm;
import com.dong8.resp.RespUser;
import com.dong8.sys.Constants;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.ToastUtil;
import com.dong8.util.Utils;
import com.loopj.android.http.RequestParams;

public class MyReserveActivity extends Activity 
{
	RespUser.User user;
	List<RespOrder.OrderForm> list = new ArrayList();
	ReserveAdapter adapter;
	ListView listview;
	TextView tvNodata;
	static boolean mPayResult = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_reserve);

		((TextView) findViewById(R.id.tv_title)).setText("我的订单");
		findViewById(R.id.ibBack).setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View arg0) 
			{
				finish();
			}
		});

		tvNodata = (TextView) findViewById(R.id.tvNodata);
		listview = (ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id)
			{
				OrderForm orderForm = list.get(position);
				Intent intent = new Intent(MyReserveActivity.this, OrderDetailActivity.class);
				intent.putExtra("orderForm", JSON.toJSONString(orderForm));
				startActivity(intent);

			}
		});
		
		mPayResult = false;
		getData();
	}

	protected void onResume()
	{
		super.onResume();
		
		if(mPayResult == true)
		{
			mPayResult = false;
			getData();
		}
	}
	
	public void getData() 
	{
		user = Utils.getUserInfo(MyReserveActivity.this);
		if (user != null) {

			MgqDataHandler loginHandler = new MgqDataHandler(this, true, false) {
				@Override
				public void onSuccess(String response) {
					super.onSuccess(response);
					RespOrder rorder = JSON.parseObject(response, RespOrder.class);
					if (rorder.code == 0) {

						list = rorder.data;

					} else {
						ToastUtil.showToastWithAlertPic(rorder.mesg);
					}
					updateUI();
				}

				public void onFailure(Throwable ble) {
				}
			};

			RequestParams params = new RequestParams();
			params.put("id", user.id);
			MgqRestClient.get(Constants.USER_ORDER, params, loginHandler);

		}

	}

	private void updateUI() {

		adapter = new ReserveAdapter(MyReserveActivity.this, list);
		listview.setAdapter(adapter);

		if (list == null || list.size() > 0) {
			tvNodata.setVisibility(View.GONE);
		} else {
			tvNodata.setVisibility(View.VISIBLE);
		}

		final Handler handle = new Handler()
		{
			public void handleMessage(Message msg)
			{
				adapter.notifyDataSetChanged();
			}
		};
		new Timer().schedule(new TimerTask()
		{
			public void run() 
			{
				handle.sendEmptyMessage(0);
			}
		}, 1000, 1000);
	}

	// class MyAdapter extends BaseAdapter {
	// public int getCount() {
	// // return m_array_list.size();
	// return 10;
	// }
	//
	// public Object getItem(int position) {
	// return null;
	// }
	//
	// public long getItemId(int position) {
	// return 0;
	// }
	//
	// public View getView(int position, View convertView, ViewGroup parent) {
	// ViewHolder holder = null;
	// if (convertView == null) {
	// holder = new ViewHolder();
	//
	// LayoutInflater mInflater = (LayoutInflater)
	// getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// convertView = mInflater.inflate(R.layout.item_reserve, null);
	//
	// convertView.setTag(holder);
	// } else {
	// holder = (ViewHolder) convertView.getTag();
	// }
	//
	// return convertView;
	// }
	// }
	//
	// public final class ViewHolder {
	// public TextView title;
	// public TextView time;
	// public TextView price;
	// public TextView order_time;
	// public ImageView img;
	//
	// public int pos;
	// }
}
