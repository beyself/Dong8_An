package com.dong8.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.adapter.HobbyAdapter;
import com.dong8.sys.Constants;
import com.dong8.sys.MyApp;
import com.dong8.type.Gym;
import com.dong8.util.PreferencesUtils;
import com.dong8.util.TLog;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class MyFavoriteActivity extends Activity {
	HobbyAdapter adapter;
	List<Gym> hList = new ArrayList<Gym>();
	ListView listview;
	TextView tvNodata;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_favorite);

		tvNodata = (TextView) findViewById(R.id.tvNodata);
		((TextView) findViewById(R.id.tv_title)).setText("收藏");
		findViewById(R.id.ibBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		final PullToRefreshListView ptrlvHeadlineNews = (PullToRefreshListView) findViewById(R.id.listview);
		ptrlvHeadlineNews.setMode(Mode.PULL_UP_TO_REFRESH);
		ptrlvHeadlineNews
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
					}
				});

		listview = ptrlvHeadlineNews.getRefreshableView();
		listview.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3) 
			{
				if(((MyApp)getApplicationContext()).mFinishActivity != null)
				{
					((MyApp)getApplicationContext()).mFinishActivity.finish();
					((MyApp)getApplicationContext()).mFinishActivity = null;
				}
				
				PreferencesUtils.putString(MyFavoriteActivity.this,"user_gym", JSON.toJSONString(hList.get(position - 1)));
				((MyApp)getApplicationContext()).mCurrentGym = hList.get(position - 1);
				startActivity(new Intent(MyFavoriteActivity.this, MainActivity.class));
				finish();
			}
		});

		updateUI();
	}

	public void onResume() {
		super.onResume();
		updateUI();
	}

	public List<Gym> getHobbyList() {
		List<Gym> hList = new ArrayList();
		String shList = PreferencesUtils.getString(MyFavoriteActivity.this,
				Constants.ID_Favorite_LIST);
		if (shList != null) {
			String[] list = shList.split(",");
			for (int i = 0; i < list.length; i++) {
				TLog.i("hansen", "====:" + list[i]);
				String temp = PreferencesUtils.getString(
						MyFavoriteActivity.this, Constants.ID_Favorite
								+ list[i]);
				Gym gym = JSON.parseObject(temp, Gym.class);
				if (gym != null) {
					hList.add(gym);
				}
			}
		}

		return hList;
	}

	private void updateUI() {

		hList = getHobbyList();
		adapter = new HobbyAdapter(MyFavoriteActivity.this, hList);
		listview.setAdapter(adapter);

		if (hList == null || hList.size() > 0) {
			tvNodata.setVisibility(View.GONE);
		} else {
			tvNodata.setVisibility(View.VISIBLE);
		}

	}
}
