package com.dong8.activity;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.adapter.MemberAdapter;
import com.dong8.resp.MyInfoResp;
import com.dong8.resp.RespUser;
import com.dong8.resp.MyInfoResp.Member;
import com.dong8.sys.Constants;
import com.dong8.sys.MyApp;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.ToastUtil;
import com.dong8.util.Utils;
import com.loopj.android.http.RequestParams;

public class MemberCardActivity extends Activity {
	RespUser.User user;
	MemberAdapter adapter;
	List<Member> mList;
	ListView listview;
	TextView tvNodata;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_member_card);

		((TextView) findViewById(R.id.tv_title)).setText("我的会员卡");
		((Button) findViewById(R.id.titleRight)).setBackgroundResource(R.drawable.btn_add_card);

		findViewById(R.id.ibBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		findViewById(R.id.titleRight).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(MemberCardActivity.this, BandMemberCardActivity.class));
			}
		});
		
		tvNodata=(TextView) findViewById(R.id.tvNodata);
		listview = (ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// ItemInfo info = items.get(position);
				// if (1 == status) {
				// if (ItemInfo.ITEM == info.type) {
				// ItemViewHolder viewHolder = (ItemViewHolder) view
				// .getTag();
				// boolean checkstate = false;
				// if (viewHolder.ckbSelectedBtn.isChecked()) {
				// viewHolder.ckbSelectedBtn.setChecked(false);
				// } else {
				// viewHolder.ckbSelectedBtn.setChecked(true);
				// }
				// checkstate = viewHolder.ckbSelectedBtn.isChecked();
				// partAdapter.ckbStatus.get(position).checkState = checkstate;
				// }
				// } else {
				// VideoPlayerActivity.play(getActivity(), info.videoId,
				// info.title);
				// }

			}
		});
	}
	
	protected void onResume()
	{
		super.onResume();
		getData();
	}
	
	private void getData()
	{
		user = Utils.getUserInfo(MemberCardActivity.this);
		if (user != null) {

			MgqDataHandler loginHandler = new MgqDataHandler(this, true, false) {
				@Override
				public void onSuccess(String response) {
					super.onSuccess(response);
					MyInfoResp myInfoResp = JSON.parseObject(response, MyInfoResp.class);
					if (myInfoResp.code == 0) {
						mList=myInfoResp.member;
						mList.get(0).money = 0;
						getMemberMoney();
					} 
					else 
					{
						ToastUtil.showToastWithAlertPic(myInfoResp.mesg);
					}
					updateUI();
				}

				public void onFailure(Throwable ble) {
					ToastUtil.showToastWithAlertPic("数据获取失败,请重试!");
				}
			};

			RequestParams params = new RequestParams();
			params.put("id", user.id);
			MgqRestClient.get(Constants.USER_MEMBER, params, loginHandler);
		}
	}
	
	private void getMemberMoney()
	{
		if(mList == null || mList.size() == 0)
		{
			return;
		}
		
		MgqDataHandler loginHandler = new MgqDataHandler(this, false, false) 
		{
			@Override
			public void onSuccess(String response) 
			{
				super.onSuccess(response);
				
				try 
				{
					JSONObject obj = new JSONObject(response);
					if(obj.getInt("code") == 0)
					{
						mList.get(0).money = obj.getJSONObject("member").getInt("cardbalance");
						adapter.notifyDataSetChanged();
					}
				} 
				catch (JSONException e) 
				{
					e.printStackTrace();
				}
			}

			public void onFailure(Throwable ble) 
			{
				ToastUtil.showToastWithAlertPic("数据获取失败,请重试!");
			}
		};

		RequestParams params = new RequestParams();
		params.put("card", mList.get(0).member_card);
		MgqRestClient.get(((MyApp)getApplicationContext()).mCurrentGym.service + Constants.CLUB_GET_MEMBER, params, loginHandler);
	}
	
	private void updateUI()
	{
		adapter = new MemberAdapter(MemberCardActivity.this, mList);
		listview.setAdapter(adapter);
		
		if(mList.size()>0){
			tvNodata.setVisibility(View.GONE);
		}else{
			tvNodata.setVisibility(View.VISIBLE);
		}
	}

	// class MyAdapter extends BaseAdapter
	// {
	// public int getCount()
	// {
	// // return m_array_list.size();
	// return 10;
	// }
	//
	// public Object getItem(int position)
	// {
	// return null;
	// }
	//
	// public long getItemId(int position)
	// {
	// return 0;
	// }
	//
	// public View getView(int position, View convertView, ViewGroup parent)
	// {
	// ViewHolder holder = null;
	// if (convertView == null)
	// {
	// holder = new ViewHolder();
	//
	// LayoutInflater mInflater = (LayoutInflater)
	// getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// convertView = mInflater.inflate(R.layout.item_member_card, null);
	//
	// convertView.setTag(holder);
	// }
	// else
	// {
	// holder = (ViewHolder) convertView.getTag();
	// }
	//
	// return convertView;
	// }
	// }
	//
	// public final class ViewHolder
	// {
	// public TextView title;
	// public TextView price;
	// public TextView time;
	//
	// public int pos;
	// }
}
