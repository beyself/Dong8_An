package com.dong8.activity;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.resp.CityList;
import com.dong8.resp.CityModel;
import com.dong8.type.City;
import com.dong8.util.PreferencesUtils;
import com.dong8.util.ResourceUtils;
import com.dong8.util.Utils;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class CityListActivity extends Activity {
	MyAdapter m_adapter;
	ArrayList<City> m_array_list = new ArrayList<City>();

	CityList cList;
	List<CityModel> mCityNames;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city_list);

		((TextView) findViewById(R.id.tv_title)).setText("选择城市");

		ListView listview = (ListView) this.findViewById(R.id.listview);
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			}
		});
		
		mCityNames = getCityList();
		
/*
		City city = new City();
		city.type = 0;
		city.city = "热门城市";
		m_array_list.add(city);
		for (int i = 0; i < 10; i++) {
			city = new City();
			city.type = 1;
			city.city = "北京";
			m_array_list.add(city);
		}

		for (char index = 'A'; index <= 'Z'; index++) {
			city = new City();
			city.type = 0;
			city.city = String.valueOf(index);
			m_array_list.add(city);
			for (int i = 0; i < 10; i++) {
				city = new City();
				city.type = 1;
				city.city = "北京";
				m_array_list.add(city);
			}
		}

		m_adapter = new MyAdapter();
		listview.setAdapter(m_adapter);

		findViewById(R.id.titleRight).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(CityListActivity.this, CityListActivity.class));
			}
		});
		
		
		getData();*/
	}
	
	private List<CityModel> getCityList() 
	{
		List<CityModel> names = new ArrayList<CityModel>();

		getData();

		if (cList != null) 
		{
			names = cList.toModelList();
		}

		return names;
	}
	
	public void getData() 
	{
		cList = Utils.getCityList(this);
		if (cList == null) 
		{
			String json = ResourceUtils.geFileFromRaw(this, R.raw.city);
			cList = JSON.parseObject(json, CityList.class);
			PreferencesUtils.putString(this, "CITY_LIST", json);
		}
	}
/*
	public void getData() {
		cList=Utils.getCityList(CityListActivity.this);
		if(cList==null){
			MgqDataHandler loginHandler = new MgqDataHandler(this, true, false) {
				@Override
				public void onSuccess(String response) {
					super.onSuccess(response);

					cList = JSON.parseObject(response, CityList.class);
					PreferencesUtils.putString(CityListActivity.this, "CITY_LIST", response);
					TLog.i("hansen", "==size   ==:"+cList.B.size());
				}

				public void onFailure(Throwable ble) {
				}
			};

			RequestParams params = new RequestParams();
			MgqRestClient.get(Constants.CITY_LIST, params, loginHandler);
		}
	}
	*/
	public void updateUI(){
		
		
		
	}
	

	class MyAdapter extends BaseAdapter {
		public int getCount() {
			return m_array_list.size();
		}

		public Object getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();

				LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.item_city, null);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			City city = m_array_list.get(position);
			if (city.type == 0) {
				holder.title.setBackgroundResource(R.drawable.bg_column);
				holder.title.setText(city.city);
				holder.title.setTextColor(Color.rgb(248, 123, 65));
			} else {
				holder.title.setBackgroundResource(R.drawable.city_bg);
				holder.title.setText("    " + city.city);
				holder.title.setTextColor(Color.rgb(109, 109, 109));
			}

			return convertView;
		}
	}

	public final class ViewHolder {
		public TextView title;
	}
}
