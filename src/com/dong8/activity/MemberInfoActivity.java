package com.dong8.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.dong8.R;
import com.dong8.resp.RespUser;
import com.dong8.util.Utils;

public class MemberInfoActivity extends Activity implements OnClickListener {
	RespUser.User user;
	TextView tvMobile;
	TextView tvEmail;
	TextView tvFavi;
	TextView tvSex;
	TextView tvNick;
	ImageView ivHead;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_member_info);

		((TextView) findViewById(R.id.tv_title)).setText("我的帐号");
		findViewById(R.id.ibBack).setOnClickListener(this);

		tvMobile = (TextView) findViewById(R.id.tvMobile);
		tvEmail = (TextView) findViewById(R.id.tvEmail);
		tvFavi = (TextView) findViewById(R.id.tvFavi);
		tvSex = (TextView) findViewById(R.id.tvSex);
		tvNick = (TextView) findViewById(R.id.tvNick);
		ivHead = (ImageView) findViewById(R.id.ivHead);

		updateUI();

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.ibBack:
			finish();
			break;
		}
	}

	public void updateUI() {
		user = Utils.getUserInfo(MemberInfoActivity.this);
		try {
			if (user != null) {

				tvNick.setText(user.nick);
				if (user.sex != null) {
					if (user.sex.equals("1")) {
						tvSex.setText("男");
					} else {
						tvSex.setText("女");
					}
				} else {
					tvSex.setText("男");
				}

				tvMobile.setText("    手机号码:" + user.phone);
				tvEmail.setText("    邮箱帐号:" + user.email);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// private void getGym(boolean show) {
	// MgqDataHandler loginHandler = new MgqDataHandler(this, show, false) {
	// @Override
	// public void onSuccess(String response) {
	// super.onSuccess(response);
	// RespGym ruser = JSON.parseObject(response, RespGym.class);
	// if (ruser.code.equals("0")) {
	//
	// } else {
	// }
	// }
	//
	// public void onFailure(Throwable ble) {
	// }
	// };
	//
	// RequestParams params = new RequestParams();
	// params.put("id", "");
	// MgqRestClient.get(Constants.USER_SHOW, params, loginHandler);
	// }
}
