package com.dong8.activity;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.resp.RespBase;
import com.dong8.sys.Constants;
import com.dong8.sys.MyApp;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.ToastUtil;
import com.dong8.util.Utils;
import com.loopj.android.http.RequestParams;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BandMemberCardActivity extends Activity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_band_member_card);

		((TextView) findViewById(R.id.tv_title)).setText("绑定会员卡");
		((Button) findViewById(R.id.titleRight)).setVisibility(View.INVISIBLE);
		
		findViewById(R.id.ibBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		findViewById(R.id.band).setOnClickListener(this);
	}

	@Override
	public void onClick(View arg0) 
	{
		switch (arg0.getId()) {
		case R.id.band:
			getData();
			break;
		}
	}

	private void getData() {
		final String card = ((EditText) findViewById(R.id.tel)).getText().toString();
		if (card.length() == 0) {
			ToastUtil.showToastWithAlertPic("请输入会员卡号");
			return;
		}

		MgqDataHandler loginHandler = new MgqDataHandler(this, true, false) {
			@Override
			public void onSuccess(String response) {
				super.onSuccess(response);
				RespBase ruser = JSON.parseObject(response.trim(), RespBase.class);
				if (ruser.code == 0) {
					ToastUtil.showToastWithOkPic("绑定成功");
					finish();
				} else {
					ToastUtil.showToastWithOkPic(ruser.mesg);
				}
			}

			public void onFailure(Throwable ble) {
			}
		};

		RequestParams params = new RequestParams();
		params.put("member_card", card);
		params.put("user_id", String.valueOf(Utils.getUserInfo(this).id));
		params.put("club_id", (((MyApp)getApplicationContext()).mCurrentGym).id);
		MgqRestClient.post(Constants.MEMBER_BAND, params, loginHandler);
	}
}
