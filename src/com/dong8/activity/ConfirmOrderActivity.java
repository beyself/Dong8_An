package com.dong8.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.resp.RespOrder.OrderForm;
import com.dong8.resp.Resp;
import com.dong8.resp.RespUser;
import com.dong8.resp.RespUser.User;
import com.dong8.resp.SpaceSection;
import com.dong8.sys.Constants;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.PreferencesUtils;
import com.dong8.util.TLog;
import com.dong8.util.ToastUtil;
import com.dong8.util.Utils;
import com.loopj.android.http.RequestParams;

public class ConfirmOrderActivity extends Activity implements OnClickListener 
{
	RespUser.User mUser;
	private TextView tvClubName;
	private TextView tvTime;
	private TextView tvSpace;
	private TextView tvPrice;
	private TextView tvDetail;
	private EditText etPerson;
	private EditText etPhone;

	OrderForm orderForm;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_order);

		((TextView) findViewById(R.id.tv_title)).setText("订单确认");
		findViewById(R.id.ibBack).setOnClickListener(this);
		findViewById(R.id.pay).setOnClickListener(this);

		tvClubName = (TextView) findViewById(R.id.tvClubName);
		tvTime = (TextView) findViewById(R.id.tvTime);
		tvSpace = (TextView) findViewById(R.id.tvSpace);
		tvPrice = (TextView) findViewById(R.id.tvPrice);
		tvDetail = (TextView) findViewById(R.id.tvDetail);
		etPerson = (EditText) findViewById(R.id.etPerson);
		etPhone = (EditText) findViewById(R.id.etPhone);

		// getIntent().getStringExtra(name)
		String sform = getIntent().getStringExtra("orderForm");
		orderForm = JSON.parseObject(sform, OrderForm.class);

		TLog.i("hansen", "===:" + sform + "  " + orderForm.amount);
		updateUI();
	}

	@Override
	public void onClick(View arg0)
	{
		switch (arg0.getId())
		{
	/*	case R.id.outlet:
			startActivity(new Intent(this, LoginActivity.class));
			break;
*/
		case R.id.ibBack:
			finish();
			break;
/*
		case R.id.pay:
			Intent intent = new Intent(this, PayOrderActivity.class);
			intent.putExtra("orderForm", JSON.toJSONString(orderForm));
			startActivity(intent);
			break;
			*/
		case R.id.pay:
			String user = PreferencesUtils.getString(this, "user");
			if(user != null && user.length() > 0)
			{
				getData();
			}
			else
			{
				startActivity(new Intent(this, LoginActivity.class));
			}
			break;
		}
	}
	
	protected void onResume()
	{
		super.onResume();
		
		String user = PreferencesUtils.getString(this,"user");
		if(user != null && user.length() > 0)
		{
			User ruser = JSON.parseObject(user, User.class);
			if(etPerson.getText().toString().length() == 0)
			{
				etPerson.setText(ruser.nick);
				etPhone.setText(ruser.phone);
			}
		}
	}
	
	private void getData() 
	{
		String name = etPerson.getText().toString();
		String tel = etPhone.getText().toString();
		
		if(name == null || tel == null || name.length() == 0 || tel.length() == 0)
		{
			ToastUtil.showToastWithOkPic("请输入预定人姓名/联系电话");
			return;
		}
		
		if(mUser == null) mUser = Utils.getUserInfo(ConfirmOrderActivity.this);
		mUser.nick = name;
		PreferencesUtils.putString(this,"user", JSON.toJSONString(mUser));
		
		MgqDataHandler loginHandler = new MgqDataHandler(this, true, false) 
		{
			@Override
			public void onSuccess(String response) 
			{
				super.onSuccess(response);
				Resp ruser = JSON.parseObject(response,Resp.class);
				if (ruser.code == 0) 
				{
					orderForm = JSON.parseObject(ruser.data,OrderForm.class);
					
					final Dialog dialog = new Dialog(ConfirmOrderActivity.this,R.style.dialog);
					dialog.setContentView(R.layout.dialog); 
					dialog.setCancelable(true);
					dialog.findViewById(R.id.confirm).setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View arg0)
						{
							Intent intent = new Intent(ConfirmOrderActivity.this, PayOrderActivity.class);
							intent.putExtra("orderForm", JSON.toJSONString(orderForm));
							startActivity(intent);
							
							dialog.dismiss();
							finish();
						}
					});
					dialog.findViewById(R.id.cancel).setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View arg0) 
						{
							dialog.dismiss();
							finish();
						}
					});
					((TextView)dialog.findViewById(R.id.msg)).setText("您的场地已经成功预定，但没有支付定金，场地可被他人抢定。是否支付订单？");
					dialog.show();
				}
				else 
				{
					
				}
			}

			public void onFailure(Throwable ble) 
			{
				
			}
		};

		String user = PreferencesUtils.getString(this,"user");
		User ruser = JSON.parseObject(user, User.class);
		orderForm.user_id = String.valueOf(ruser.id);
		orderForm.cust_name = etPerson.getText().toString();
		orderForm.phone = etPhone.getText().toString();
		orderForm.user_name = ruser.username;
		orderForm.create_time = System.currentTimeMillis();
		
		RequestParams params = new RequestParams();
		params.put("data", JSON.toJSONString(orderForm));
		MgqRestClient.post(Constants.INIT_ORDER, params, loginHandler);
	}

	private void updateUI() 
	{
		try
		{
			mUser = Utils.getUserInfo(ConfirmOrderActivity.this);
			if (orderForm != null)
			{
				tvClubName.setText(orderForm.club_name);
				tvPrice.setText("￥" + orderForm.amount);
				String startTime = SpaceSection.getStartDate(orderForm.order_item);
				tvTime.setText(startTime);
				String info = SpaceSection.getSpaceInfo(orderForm.order_item);
				tvDetail.setText(info);
				tvSpace.setText(orderForm.order_desc);

				etPerson.setText(mUser.nick);
				etPhone.setText(mUser.phone);
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
