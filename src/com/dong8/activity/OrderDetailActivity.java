package com.dong8.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.adapter.ReserveAdapter;
import com.dong8.resp.Resp;
import com.dong8.resp.RespUser;
import com.dong8.resp.SpaceSection;
import com.dong8.resp.RespOrder.OrderForm;
import com.dong8.resp.RespUser.User;
import com.dong8.sys.Constants;
import com.dong8.sys.MyApp;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.PreferencesUtils;
import com.dong8.util.TLog;
import com.dong8.util.ToastUtil;
import com.dong8.util.Utils;
import com.loopj.android.http.RequestParams;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class OrderDetailActivity extends Activity implements OnClickListener
{
	OrderForm orderForm;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order_detail);

		((TextView) findViewById(R.id.tv_title)).setText("订单详情");
		findViewById(R.id.ibBack).setOnClickListener(this);
		findViewById(R.id.pay).setOnClickListener(this);
		findViewById(R.id.cancel).setOnClickListener(this);

		// getIntent().getStringExtra(name)
		String sform = getIntent().getStringExtra("orderForm");
		orderForm = JSON.parseObject(sform, OrderForm.class);
		updateUI();
		
		((MyApp)getApplicationContext()).mFinishActivity = this;
	}

	@Override
	public void onClick(View arg0)
	{
		switch (arg0.getId())
		{
	/*	case R.id.outlet:
			startActivity(new Intent(this, LoginActivity.class));
			break;
*/
		case R.id.ibBack:
			finish();
			break;
/*
		case R.id.pay:
			Intent intent = new Intent(this, PayOrderActivity.class);
			intent.putExtra("orderForm", JSON.toJSONString(orderForm));
			startActivity(intent);
			break;
			*/
		case R.id.pay:
			Intent intent = new Intent(this, PayOrderActivity.class);
			intent.putExtra("orderForm", JSON.toJSONString(orderForm));
			startActivity(intent);
			break;
		}
	}
	
	protected void onResume()
	{
		super.onResume();
	}

	private void updateUI() 
	{
		((TextView) findViewById(R.id.tvClubName)).setText(orderForm.club_name);
		((TextView) findViewById(R.id.tvPrice)).setText("￥" + orderForm.amount);
		String startTime = SpaceSection.getStartDate(orderForm.order_item);
		((TextView) findViewById(R.id.tvTime)).setText(startTime);
		String info = SpaceSection.getSpaceInfo(orderForm.order_item);
		((TextView) findViewById(R.id.tvDetail)).setText(info);
		((TextView) findViewById(R.id.tvSpace)).setText(orderForm.order_desc);
		
		((TextView) findViewById(R.id.etPerson)).setText(orderForm.cust_name);
		((TextView) findViewById(R.id.etPhone)).setText(orderForm.phone);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = null;
		try 
		{
			date = sdf.parse(SpaceSection.getStartTime(orderForm.order_item));
		}
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		
		int sec = (int)(System.currentTimeMillis()/1000);
		final int oldSec = (int)(date.getTime()/1000);
		int offset = oldSec - sec;
		
		if(orderForm.status.equals("3"))
		{
			findViewById(R.id.pay_layout).setVisibility(View.GONE);
			findViewById(R.id.cancel_layout).setVisibility(View.GONE);
			findViewById(R.id.left_time).setVisibility(View.VISIBLE);
			
			if(offset < 0)
			{
				((TextView)findViewById(R.id.left_time)).setText("已完成");
			}
			else
			{
				final Handler handler = new Handler()
				{
					public void handleMessage(Message msg)
					{
						((TextView)findViewById(R.id.left_time)).setText((String)msg.obj);
					}
				};
				
				new Timer().schedule(new TimerTask()
				{
					@Override
					public void run() 
					{
						int sec = (int)(System.currentTimeMillis()/1000);
						int offset = oldSec - sec;
						
						StringBuffer buffer = new StringBuffer();
						int hour = offset/3600;
						buffer.append(hour + "时");
			
						int min = (offset - hour*3600)/60;
						buffer.append(min + "分");

						sec = offset%60;
						buffer.append(sec + "秒");
						
						handler.sendMessage(handler.obtainMessage(0, buffer.toString()));
					}
				}, 1000, 1000);
				
				StringBuffer buffer = new StringBuffer();
				int hour = offset/3600;
				buffer.append(hour + "时");
	
				int min = (offset - hour*3600)/60;
				buffer.append(min + "分");

				sec = offset%60;
				buffer.append(sec + "秒");
				
				((TextView)findViewById(R.id.left_time)).setText(buffer.toString());
			}
		}
		else
		{
			if(offset < 0)
			{
				findViewById(R.id.pay_layout).setVisibility(View.GONE);
				findViewById(R.id.cancel_layout).setVisibility(View.GONE);
				findViewById(R.id.left_time).setVisibility(View.VISIBLE);
				
				((TextView)findViewById(R.id.left_time)).setText("已失效");
			}
			else
			{
				findViewById(R.id.cancel_layout).setVisibility(View.VISIBLE);
				findViewById(R.id.pay_layout).setVisibility(View.VISIBLE);
				findViewById(R.id.left_time).setVisibility(View.GONE);
			}
		}
	}
}
