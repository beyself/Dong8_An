package com.dong8.activity;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.resp.RespGymDetail;
import com.dong8.sys.Constants;
import com.dong8.sys.MyApp;
import com.dong8.type.DetailGymType;
import com.dong8.type.Gym;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.TLog;
import com.loopj.android.http.RequestParams;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class FieldActivity extends Activity 
{
	private List<DetailGymType> movetypeList = null;// 该场馆的运动类型列表
	Gym mGym;
	MyListAdapter m_adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_field);
		
		mGym = ((MyApp)getApplicationContext()).mCurrentGym;
		
		ListView listview = (ListView) findViewById(R.id.listview);
		listview.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,int position, long id) 
			{
				if (movetypeList != null && movetypeList.size() > 0) 
				{
					DetailGymType dgt = (DetailGymType) movetypeList.get(position);

					Intent intent = new Intent(FieldActivity.this,OrderFieldActivity.class);
					intent.putExtra("clubId", dgt.clubId);
					intent.putExtra("fmtype_code", dgt.fmtype_code);
					intent.putExtra("fclub_code", mGym.fclub_code);
					intent.putExtra("service", mGym.service);
					intent.putExtra("club_name", mGym.clubName);
					intent.putExtra("mtypeId", dgt.mtypeId);
					startActivity(intent);
				}
			}
		});
		m_adapter = new MyListAdapter();
		listview.setAdapter(m_adapter);
		
		getData(true);
	}
	
	class MyListAdapter extends BaseAdapter {
		public int getCount() {
			if (movetypeList == null)
				return 0;
			return movetypeList.size();
		}

		public Object getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();

				LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.item_field, null);
//				holder.price = (TextView) convertView.findViewById(R.id.price);
				holder.title = (TextView) convertView.findViewById(R.id.title);
				holder.img = (ImageView) convertView.findViewById(R.id.img);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			DetailGymType temp = (DetailGymType) movetypeList.get(position);
			TLog.i("hansen", "=======::" + temp.mtypeId);

			if (temp.mtypeId.equals("badminton")) {
				holder.title.setText("羽毛球");
			} else if (temp.mtypeId.equals("tableTennis")) {
				holder.title.setText("乒乓球");
			}else if (temp.mtypeId.equals("bowling")) {
				holder.title.setText("保龄球");
			}

			if (temp.mtypeId.equals(Constants.TY_archery)) {
				holder.img.setBackgroundResource(R.drawable.ic_archery);
			} else if (temp.mtypeId.equals(Constants.TY_badminton)) {
				holder.img.setBackgroundResource(R.drawable.ic_badminton);
			} else if (temp.mtypeId.equals(Constants.TY_basketball)) {
				holder.img.setBackgroundResource(R.drawable.ic_basketball);
			} else if (temp.mtypeId.equals(Constants.TY_bicycle)) {
				holder.img.setBackgroundResource(R.drawable.ic_bicycle);
			} else if (temp.mtypeId.equals(Constants.TY_billiards)) {
				holder.img.setBackgroundResource(R.drawable.ic_billiards);
			} else if (temp.mtypeId.equals(Constants.TY_bowling)) {
				holder.img.setBackgroundResource(R.drawable.s8);
			} else if (temp.mtypeId.equals(Constants.TY_car)) {
				holder.img.setBackgroundResource(R.drawable.ic_car);
			} else if (temp.mtypeId.equals(Constants.TY_darts)) {
				holder.img.setBackgroundResource(R.drawable.ic_darts);
			} else if (temp.mtypeId.equals(Constants.TY_equestrian)) {
				holder.img.setBackgroundResource(R.drawable.ic_equestrian);
			} else if (temp.mtypeId.equals(Constants.TY_football)) {
				holder.img.setBackgroundResource(R.drawable.ic_football);
			} else if (temp.mtypeId.equals(Constants.TY_golf)) {
				holder.img.setBackgroundResource(R.drawable.ic_golf);
			} else if (temp.mtypeId.equals(Constants.TY_kart)) {// -----------有问题
				holder.img.setBackgroundResource(R.drawable.ic_car);
			} else if (temp.mtypeId.equals(Constants.TY_roller)) {
				holder.img.setBackgroundResource(R.drawable.ic_roller);
			} else if (temp.mtypeId.equals(Constants.TY_shooting)) {
				holder.img.setBackgroundResource(R.drawable.ic_shooting);
			} else if (temp.mtypeId.equals(Constants.TY_skating)) {
				holder.img.setBackgroundResource(R.drawable.ic_skating);
			} else if (temp.mtypeId.equals(Constants.TY_ski)) {
				holder.img.setBackgroundResource(R.drawable.ic_ski);
			} else if (temp.mtypeId.equals(Constants.TY_squash)) {// -----------有问题
				holder.img.setBackgroundResource(R.drawable.s8);
			} else if (temp.mtypeId.equals(Constants.TY_swimming)) {
				holder.img.setBackgroundResource(R.drawable.ic_swimming);
			} else if (temp.mtypeId.equals(Constants.TY_tableTennis)) {
				holder.img.setBackgroundResource(R.drawable.ic_tabletennis);
			} else if (temp.mtypeId.equals(Constants.TY_tennis)) {
				holder.img.setBackgroundResource(R.drawable.ic_tennis);
			}
			
//			holder.price.setText("会员价: ￥80    \n非会员: ￥100    ");
			return convertView;
		}
	}

	public final class ViewHolder {
		public TextView title;
//		public TextView content;
//		public TextView price;
		public ImageView img;
	}
	
	private void getData(boolean show) {
		MgqDataHandler loginHandler = new MgqDataHandler(this, show, false) {
			@Override
			public void onSuccess(String response) 
			{
				super.onSuccess(response);
				TLog.i("hansen", "=======response==" + response);
				RespGymDetail ruser = JSON.parseObject(response,
						RespGymDetail.class);
				if (ruser.code == 0) {
					movetypeList = ruser.movetypeList;
					m_adapter.notifyDataSetChanged();
					
					if(movetypeList.size() == 0) findViewById(R.id.tvNodata).setVisibility(View.VISIBLE);
					else findViewById(R.id.tvNodata).setVisibility(View.GONE);
				}
				else 
				{
					
				}
			}

			public void onFailure(Throwable ble) 
			{
			}
		};

		RequestParams params = new RequestParams();
		params.put("id", mGym.id);
		MgqRestClient.get(Constants.CLUB_DETAIL, params, loginHandler);
	}
	
	@Override
	public void onBackPressed() {
		this.getParent().onBackPressed();
	}
}
