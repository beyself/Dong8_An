package com.dong8.activity;

import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.resp.RespUser;
import com.dong8.sys.Constants;
import com.dong8.sys.MyApp;
import com.dong8.util.MgqDataHandler;
import com.dong8.util.MgqRestClient;
import com.dong8.util.PreferencesUtils;
import com.dong8.util.ToastUtil;
import com.loopj.android.http.RequestParams;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class VerifyCodeActivity extends Activity implements OnClickListener
{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_verify_code);
		
		((TextView)findViewById(R.id.tv_title)).setText("输入验证码&设置密码");
		findViewById(R.id.ibBack).setOnClickListener(this);
		findViewById(R.id.login).setOnClickListener(this);
		
		((MyApp)getApplicationContext()).addActivity(this);
		ToastUtil.showToastWithAlertPic("验证码已通过短信发送到您注册的手机号码");
	}

	protected void onDestroy()
	{
		super.onDestroy();
		((MyApp)getApplicationContext()).removeActivity(this);
	}

	@Override
	public void onClick(View arg0) 
	{
		switch(arg0.getId())
		{
		case R.id.ibBack:
			finish();
			break;
			
		case R.id.login:
			getData();
			break;
		}
	}
	
	private void getData()
	{
		final String code = ((EditText)findViewById(R.id.code)).getText().toString();
		final String pwd = ((EditText)findViewById(R.id.pwd)).getText().toString();
		final String pwd0 = ((EditText)findViewById(R.id.pwd_0)).getText().toString();
		
		if(!code.equals(getIntent().getExtras().getString("code")))
		{
			ToastUtil.showToastWithAlertPic("验证码没有输入/输入不正确");
			return;
		}
		
		if(pwd.length() == 0 || pwd0.length() == 0 || !pwd.equals(pwd0))
		{
			ToastUtil.showToastWithAlertPic("密码不能为空/两次密码输入不一致");
			return;
		}
		
		MgqDataHandler loginHandler = new MgqDataHandler(this, true, false) 
		{
			@Override
			public void onSuccess(String response) 
			{
				super.onSuccess(response);
				RespUser respUser = JSON.parseObject(response, RespUser.class);
				if (respUser.code==0) 
				{
					PreferencesUtils.putString(VerifyCodeActivity.this,"user", JSON.toJSONString(respUser.data.get(0)));
					((MyApp)getApplicationContext()).exit();
					ToastUtil.showToastWithAlertPic("注册成功");
					finish();
				} 
				else 
				{
					ToastUtil.showToastWithAlertPic(respUser.mesg);
				}
			}

			public void onFailure(Throwable ble) 
			{
			}
		};
		
		JSONObject obj = new JSONObject();
		try 
		{
			obj.put("phone", getIntent().getExtras().getString("tel"));
			obj.put("username", getIntent().getExtras().getString("tel"));
			obj.put("password", pwd);
		}
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
		RequestParams params = new RequestParams();
		params.put("data", obj.toString());
		MgqRestClient.get(Constants.USER_REGISTER, params, loginHandler);
	}
}
