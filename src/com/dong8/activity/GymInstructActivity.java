package com.dong8.activity;

import com.dong8.R;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class GymInstructActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gym_instruct);
		
		findViewById(R.id.ibBack).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				finish();
			}
		});
		((TextView)findViewById(R.id.tv_title)).setText("场馆详细介绍");
		
		((TextView)findViewById(R.id.text)).setText(getIntent().getExtras().getString("desc"));
		((TextView)findViewById(R.id.tv_title)).setText(getIntent().getExtras().getString("title"));
	}
}
