package com.dong8.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import com.alibaba.fastjson.JSON;
import com.dong8.resp.CityList;
import com.dong8.resp.RespUser;

public class Utils {
	public static final String PACKAGE_NAME = "com.dong8";

	@SuppressWarnings("unused")
	public static String getClientVersionHead(Context context) {

		String pName = PACKAGE_NAME;
		String versionName = getVersionName(context);
		return "Android|" + versionName;
	}

	public static String getVersionName(Context context) {
		String versionName = "1.0";
		try {
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(PACKAGE_NAME,
					PackageManager.GET_CONFIGURATIONS);
			versionName = pinfo.versionName;
		} catch (NameNotFoundException e) {

		}

		return versionName;
	}

	public static int getVersionCode(Context context) {

		int versionCode = 8;
		try {
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(PACKAGE_NAME,
					PackageManager.GET_CONFIGURATIONS);
			versionCode = pinfo.versionCode;
		} catch (NameNotFoundException e) {

		}

		return versionCode;
	}

	public static RespUser.User getUserInfo(Context context) {
		String sss = PreferencesUtils.getString(context, "user", "");
		RespUser.User user = null;
		TLog.i("hansen", "=========user===:"+sss);
		try {
			user = (RespUser.User) JSON.parseObject(sss, RespUser.User.class);

		} catch (Exception e) {
			e.printStackTrace();
			return  null;
		}
		return user;
	}
	
	/**
	 * 获取城市列表信息  
	 * @param context
	 * @return
	 */
	public static CityList  getCityList(Context context) {
		String sss = PreferencesUtils.getString(context, "CITY_LIST", "");
		CityList cityList=null;
		try {
			cityList = (CityList) JSON.parseObject(sss, CityList.class);

		} catch (Exception e) {
			e.printStackTrace();
			return  null;
		}
		return cityList;
	}
	
	
	
	/**
	 * 获取当前选择的城市
	 * @param context
	 * @return
	 */
	public static String  getCityString(Context context) {
		return PreferencesUtils.getString(context, "city", "");
		
	}

	private static long lastClickTime;
    public static boolean isFastDoubleClick() 
    {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if ( 0 < timeD && timeD < 1000) 
        {   
            return true;   
        }
        lastClickTime = time;   
        return false;   
    }
}
