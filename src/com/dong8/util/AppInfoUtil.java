/*
 * File Name:  AppInfoUtil.java
 * Copyright:  China Academy of Telecommunication Research of HUNANTV Holdings Limited 2012,  All rights reserved
 * Created By:  yinb
 * Create Time:  2014-1-27 下午1:09:06
 */
package com.dong8.util;

import com.dong8.sys.MyApp;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

/**
 * 
 * @author yinb
 * @version [V1.0.0.0, 2014-1-27]
 */
public class AppInfoUtil {
	public static final String TAG = "AppInfoUtil";
	private static String mVersionName;
	private static int mVersionCode;
	private static String mImsi;
	private static String mImei;
	private static String deviceId;

	public static final int MAX_MQTT_CLIENTID_LENGTH = 22;
	public static final int MAX_DEVICEID_LENGTH = 16;

	private static int cpuNum;

	/**
	 * 获取版本信息</br>
	 */
	public static String getVersionName() {
		if (mVersionName == null) {
			try {
				PackageInfo pInfo = MyApp.getContext().getPackageManager().getPackageInfo(MyApp.getContext().getPackageName(), PackageManager.GET_CONFIGURATIONS);
				mVersionName = pInfo.versionName;
			} catch (Exception e) {
				e.printStackTrace();
				return "1.0";
			}
		}
		return mVersionName;
	}

	/**
	 * 获取版本号
	 */
	public static int getVersionCode() {
		if (mVersionCode <= 0) {
			try {
				PackageInfo pInfo = MyApp.getContext().getPackageManager().getPackageInfo(MyApp.getContext().getPackageName(), PackageManager.GET_CONFIGURATIONS);
				mVersionCode = pInfo.versionCode;
			} catch (Exception e) {
				e.printStackTrace();
				mVersionCode = 0;
			}
		}
		return mVersionCode;
	}

	/**
	 * 获取手机sim卡imsi码
	 */
	public static String getImsi() {
		if (mImsi == null) {
			TelephonyManager telephonyManager = (TelephonyManager) MyApp.getContext().getSystemService(Context.TELEPHONY_SERVICE);
			mImsi = telephonyManager.getSubscriberId();
		}
		return mImsi;
	}

	/**
	 * 获取手机imei码
	 */
	public static String getImei() {
		if (mImei == null) {
			TelephonyManager telephonyManager = (TelephonyManager) MyApp.getContext().getSystemService(Context.TELEPHONY_SERVICE);
			mImei = telephonyManager.getDeviceId();
		}
		return mImei;
	}

	/**
	 * 获取应用ticket,若没有，则返回“”
	 */
/*	public static String getTicket() {
		String ticket = PreferencesUtil.getString(Constants.PREF_KEY_TICKET, "");
		return ticket;
	}
*
	/**
	 * 获取用户中存储的用户名，没有则返回“”
	 */

	/** 获取手机型号 */
	public static String getModel() {
		return android.os.Build.MODEL;
	}

	/** 获取系统版本 */
	public static String getOsVersion() {
		return android.os.Build.VERSION.RELEASE;
	}

	/** 获取设备id */
	// public static String getDeviceId() {
	// if (deviceId == null) {
	// TelephonyManager telephonyManager = (TelephonyManager)
	// MyApp.getContext().getSystemService(
	// Context.TELEPHONY_SERVICE);
	// LogUtil.i("hansen",
	// "=======in getDeviceId==="+telephonyManager.getDeviceId());
	// deviceId = telephonyManager.getDeviceId();
	// }
	// return deviceId;
	// }

	public static String getDeviceId() {
		TelephonyManager tm = (TelephonyManager) MyApp.getContext().getSystemService("phone");
		String deviceID = tm.getDeviceId();
		if (deviceID == null || deviceID.equals("")) {
			deviceID = Secure.getString(MyApp.getContext().getContentResolver(), Secure.ANDROID_ID);
			if (deviceID == null || deviceID.equals("")) {
				deviceID = "";
			} else {
				// deviceID = "q" + deviceID;
			}
		}
		deviceID = "i" + deviceID;
		if (deviceID.length() > MAX_DEVICEID_LENGTH) {
			deviceID = deviceID.substring(0, MAX_DEVICEID_LENGTH);
		}
		return deviceID;
	}
}
