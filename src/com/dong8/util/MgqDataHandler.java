package com.dong8.util;

import org.apache.http.Header;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;

import com.loopj.android.http.AsyncHttpResponseHandler;

public class MgqDataHandler extends AsyncHttpResponseHandler {
	public final static String TAG = "MgqDataHandler";
	private Context context;
	private boolean showDialog;
	private boolean showError;
	@SuppressWarnings("unused")
	private boolean clearCookie;
	private ProgressDialog progressDialog;

	public boolean isShowDialog() {
		return showDialog;
	}

	public void setShowDialog(boolean showDialog) {
		this.showDialog = showDialog;
	}

	public boolean isShowError() {
		return showError;
	}

	public void setShowError(boolean showError) {
		this.showError = showError;
	}

	public MgqDataHandler(Context ctx, boolean showDialog, boolean clearCookie) {

		this.showDialog = true;
		this.showError = true;
		context = ctx;
		this.showDialog = showDialog;

	}

	public void onFailed(Throwable ee) {
		TLog.i(TAG, "===in  onFailed==");
	}

	public void onStart() {
		TLog.i(TAG, "===in  onStart==");
		if (checkNetwork()) {

			if (showDialog) {
				progressDialog = ProgressDialog.show(context, "", "正在加载中...", true, false);
				progressDialog.setCancelable(true);
			}
		} else {
			TLog.i(TAG, "===in  onStart==" + " net work  is  error====");
			ToastUtil.showToastWithAlertPic("网络连接不可用");
		}
		super.onStart();
	}

	public void onFailure(Throwable ble) {
		TLog.i(TAG, "===in  onFailure==" + ble.getMessage());
		ToastUtil.showToastWithAlertPic("数据请求异常,请稍后再试");
	}

	public void onSuccess(String response) 
	{
		TLog.d("json str", response);
		TLog.i(TAG, "===in  onSuccess==");
		try {

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (showDialog) {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
		}
	}

	// public void onSuccess(String response);

	private boolean checkNetwork() {

		String service = Context.CONNECTIVITY_SERVICE;
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(service);
		NetworkInfo activeNetwork = connectivity.getActiveNetworkInfo();
		if (null == activeNetwork) {
			return false;
		}
		return activeNetwork.isConnected();
	}

	@Override
	public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
		TLog.d(TAG, "http request success");
		try {
			String response = new String(responseBody, "UTF-8");
			TLog.d("response json:", response);
			onSuccess(response);
			/*
			// TODO - 主线程解码，阻塞，心增加线程解码 beyself 2014-08-23
			final Handler handle = new Handler()
			{
				public void handleMessage(Message msg)
				{
					onSuccess((String)msg.obj);
				}
			};
			
			final String resp = response;
			new Thread()
			{
				public void run()
				{
					handle.sendMessage(handle.obtainMessage(0, XXTEA.cj(resp, "1234567890abcdef")));
				}
			}.start();*/
			// TODO-如果response全是json对象则可以对需要做认证的网络进行判断
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
		TLog.d(TAG, "http request failed");
		if (showDialog) {
			if (NetworkProber.isNetworkAvailable(context)) {
				ToastUtil.showToastWithAlertPic("数据请求异常,请稍后再试");
			} else {
				ToastUtil.showToastWithAlertPic("网络连接不可用");
			}
		}
	}

	@Override
	public void onCancel() {
		super.onCancel();
		TLog.d(TAG, "http request canceled");
		dismissLoading();
	}

	@Override
	public void onProgress(int bytesWritten, int totalSize) {
		super.onProgress(bytesWritten, totalSize);
		TLog.d(TAG, "http request onProgress");
	}

	@Override
	public void onRetry(int retryNo) {
		super.onRetry(retryNo);
		TLog.d(TAG, "http request retry");
	}

	@Override
	public void onFinish() {
		super.onFinish();
		TLog.d(TAG, "http request finished");
		// Completed the request (either success or failure)
		dismissLoading();
	}

	private void dismissLoading() {
		if (showDialog) {
			if (progressDialog != null) {
				progressDialog.dismiss();
			}
		}
	}

}
