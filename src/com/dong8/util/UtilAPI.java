package com.dong8.util;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;

public class UtilAPI 
{
	static public double gps2m(double lat_a, double lng_a, double lat_b, double lng_b) 
	{
		double meter = DistanceUtil. getDistance(new LatLng(lat_a,lng_a),new LatLng(lat_b, lng_b));
		return meter/1000;
	}
}
