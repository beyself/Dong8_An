package com.dong8.util;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


public class MgqRestClient {
	public final static String TAG = "MgqRestClient";

	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		if(params!=null)
		{
			params.put("appVersion", AppInfoUtil.getVersionName());
			params.put("token", "");
			params.put("osType", AppInfoUtil.getOsVersion());
			params.put("deviceid", AppInfoUtil.getDeviceId());
		}
		client.setTimeout(30000);
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		if(params!=null)
		{
			params.put("appVersion", AppInfoUtil.getVersionName());
			params.put("token", "");
			params.put("osType", AppInfoUtil.getOsVersion());
			params.put("deviceid", AppInfoUtil.getDeviceId());
		}
		
		client.setTimeout(60000);
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return relativeUrl;
	}
}
