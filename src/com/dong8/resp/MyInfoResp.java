package com.dong8.resp;

import java.util.List;

import com.dong8.type.Gym;

/**
 * 4天内的
 * 
 * @author hansen
 * 
 */
public class MyInfoResp extends RespBase {

	public List<Gym> hobby;
	public List<Member> member;

	public static class Member {
		public int id;
		public int status;
		public int user_id;
		public int club_id;
		public String card_desc;
		public long ctime;
		public String member_card;
		public int money;

	}

}
