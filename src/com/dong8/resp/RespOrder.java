package com.dong8.resp;

import java.util.List;

public class RespOrder extends RespBase{
	public List<OrderForm> data;
	
	public static class OrderForm {
		public int id;
		public int amount;
		public int club_id;
		public String club_name;
		public long create_time;
		public String cust_name;
		public String order_code;
		public String order_desc;
		public String order_item;
		public String phone;
		public String status;
		public String user_id;
		public String user_name; 
		
	}
}
