package com.dong8.resp;

import java.util.Date;


/**
 * 订单
 * 生成订单，是客户端主动发起的， 服务端要自动生成，id，status=0， 
 * @author hansen
 *
 */
public class Order {

	public int id;// 自动生成
	public int user_id;
	public int club_id;
	public String user_name;
	public String club_name;
	public int amount;
	public int status;//  =0 创建的订单未支付， =1，已经支付的订单， =4 废弃或失效的订单 


	/**
	 * 	 SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddhhmmssSSS")
	 * 服务端自动生成，规则 club_id+D8+时间（20140605223344123  到毫秒）+最后2位*97的数字， 先这样做把，
	 * 如：01D8201406052233441232231， 即01+D8+20140605223344123 +2231(23*97 )
	 */
	public String order_code;//
	public String ipay_code;// 是支付成功的时候才会有 ，
	public String space_code;// 客户端传
	public String space_name;// 客户端传
	public String section_code;// 客户端传
	public String section_name;// 客户端传
	
	public Date create_time;// 创建时间 创建时系统时间
	public Date pay_time;// 支付时间 支付时系统时间
	
	public String phone;// 联系电话  客户端传
	public String order_desc;// 订单描述 客户端传
	public String member_card;// 客户端传
	
	
}
