package com.dong8.resp;

/**
 * ��������ʵ����
 * 
 * @author sy
 * 
 */
public class CityModel {
	public String cityName; // 城市名字
	public String nameSort; // 城市首字母
	public String cityCode;
	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getNameSort() {
		return nameSort;
	}
	public void setNameSort(String nameSort) {
		this.nameSort = nameSort;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
}
