package com.dong8.resp;

import java.util.ArrayList;
import java.util.List;

import com.dong8.type.DetailGymType;
import com.dong8.type.Gym;
import com.dong8.type.GymImg;

public class RespGymDetail extends Resp 
{
	public Gym club;
	public List<GymImg> picList = new ArrayList<GymImg>();
	public List<DetailGymType> movetypeList = new ArrayList<DetailGymType>();
}
