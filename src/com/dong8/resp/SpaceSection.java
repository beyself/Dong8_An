package com.dong8.resp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.alibaba.fastjson.JSON;

@SuppressWarnings("rawtypes")
public class SpaceSection implements Comparable {

	// String
	// ss="['2014-07-06','01','01','3145-0110-20:00-10','3144-0110-20:30-10','3143-0110-21:00-10']";
	String spid;
	String spname;
	String stid;
	String stname;

	public SpaceSection(String stid, String spid, String stname, String spname) {
		this.spid = spid;
		this.spname = spname;
		this.stid = stid;
		this.stname = stname;
	}

	/**
	 * 返回订单需要的描述字符串
	 * 
	 * @param list
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String toString(List<SpaceSection> list) {
		Collections.sort(list);

		// for(int i=0;i<list.size();i++){
		// SpaceSection ss = (SpaceSection) list.get(i);
		//
		// System.out.println("=====::"+ss.spname+ "  "+ss.stname);
		// }

		String spname = "";
		String result = "";
		for (int i = 0; i < list.size(); i++) {
			SpaceSection ss = (SpaceSection) list.get(i);

			if (!ss.spname.equals(spname)) {// 不相同，需要加场地
				spname = ss.spname;
				if(i == 0) result = result + "场地: " + spname + "\n时间: " + ss.stname;
				else result = result + "\n\n场地: " + spname + "\n时间: " + ss.stname;
			} else {
				result = result + " " + ss.stname;
			}

		}

		return result;
	}

	/**
	 * 从List中获取 最小的时间段
	 * 
	 * @param list
	 * @return
	 */
	public static String minTime(List<SpaceSection> list) {

		String time = ((SpaceSection) list.get(0)).stname;
		for (int i = 1; i < list.size(); i++) {

			SpaceSection tempss = (SpaceSection) list.get(i);

			String temp = tempss.stname;
			if (temp.compareTo(time) < 0) {
				time = temp;
			}
		}

		return time;

	}

	/**
	 * 将json 转换为 List数组
	 * 
	 * @param json
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List jsonToList(String json) {

		String[] dlist = JSON.parseObject(json, String[].class);
		List sList = new ArrayList();

		for (int i = 3; i < dlist.length; i++) {
			String[] aa = dlist[i].split("-");
			SpaceSection temp = new SpaceSection(aa[0], aa[1], aa[2], aa[3]);
			sList.add(temp);
		}

		return sList;
	}

	public static String getStartDate(String json) {

		String[] dlist = JSON.parseObject(json, String[].class);
		return dlist[0];
	}

	@Override
	public int compareTo(Object o) {
		SpaceSection ss = (SpaceSection) o;
		int aa = this.spname.compareTo(ss.spname);
		if (aa == 0) {
			int bb = this.stname.compareTo(ss.stname);
			return bb;
		} else {
			return aa;
		}

	}

	@SuppressWarnings("unchecked")
	public static String getStartTime(String json) {
		List sList = SpaceSection.jsonToList(json);
		String result = getStartDate(json) + " " + minTime(sList);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static String getSpaceInfo(String json){
		List sList = SpaceSection.jsonToList(json);
		return SpaceSection.toString(sList);
	}
	
	@SuppressWarnings("unchecked")
	public static String getSingleSpaceInfo(String json){
		List sList = SpaceSection.jsonToList(json);
		Collections.sort(sList);
		return "场地" + ((SpaceSection) sList.get(0)).spname;
	}

/*
	public static void main(String[] args) {
		String json = "['2014-07-06','01','01','3145-0110-20:00-10','3144-0110-20:30-10','3143-0110-21:00-11','3143-0110-06:00-10']";

		List sList = SpaceSection.jsonToList(json);

		System.out.println("====最小时间：" + SpaceSection.minTime(sList));
		System.out.println("====订单详情：" + SpaceSection.toString(sList));

	}*/
}
