package com.dong8.resp;

import java.util.List;

/**
 * 场地、时间段的列表数据
 * @author hansen
 *
 */
public class SpaceInfo extends Resp { 

	public List<Section > sectionList;
	public List<Space > spaceList;
	
	public static class Section {
		/**频道名*/
		public int sid;
		/**频道地址*/
		public int sequence;
		/**时间段*/
		public String period;
	}
	
	
	public static class Space {

		public int sequence;
		
		public String item_code;
		
		/**
		 * 场地名称   1号场地
		 */
		public String item_name;
	}
	

}
