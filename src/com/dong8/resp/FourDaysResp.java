package com.dong8.resp;

import java.util.List;

/**
 * 4天内的
 * @author hansen
 *
 */
public class FourDaysResp extends RespBase{
	public String fclut_code;
	public String movetype;
	public String startDate;
	//-----------以上3个是输入参数的返回 
	
	public List<String> d1;
	public List<String> d2;
	public List<String> d3;
	public List<String> d4;

	
	/*
	public static class Info {
		public int sid;
		public int res_item;// 时间段的id
		public String res_time;//时间段的名称
		public String cardno;
		public String cname;// 客户名称  我们这里是手机号
		public int status;// 状态
	}*/
}
