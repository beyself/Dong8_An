package com.dong8.resp;

import java.util.List;

public class RespUser extends RespBase{
	public List<User> data;
	
	public static class User {
		public int id;
		public String area;//时间段的名称
		public String bak;
		public String description;
		public String email;
		public String hobby;
		public String img;
		public String nick;
		public String phone;
		public String qq;
		public long registerDate;
		public String sex;
		public String token;
		public String username;
		public String weixin;
		public int status;// 状态
		
	
	}
}
