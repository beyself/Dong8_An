package com.dong8;

import java.util.Timer;
import java.util.TimerTask;
import com.dong8.activity.MainActivity;
import com.dong8.activity.RecommendGymActivity;
import com.dong8.sys.MyApp;
import com.umeng.analytics.MobclickAgent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;

@SuppressLint("HandlerLeak")
public class Dong8Activity extends Activity 
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dong8);
		
		final Handler handler = new Handler() 
		{
			public void handleMessage(Message msg) 
			{
				if(((MyApp)getApplicationContext()).mCurrentGym != null)
				{
					startActivity(new Intent(Dong8Activity.this, MainActivity.class));
				}
				else
				{
					startActivity(new Intent(Dong8Activity.this, RecommendGymActivity.class));
				}
				
				finish();
				super.handleMessage(msg);
			}
		};

		new Timer().schedule(new TimerTask() 
		{
			@Override
			public void run() {
				handler.sendEmptyMessage(0);
			}
		}, 2000);
	}

	public void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	public void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
