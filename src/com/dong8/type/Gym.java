package com.dong8.type;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Gym implements Serializable
{
	public String account;
	public String address;
	public String city;
	public String clubCode;
	public String clubEnName;
	public String clubName;
	public String companyId;
	public String description;
	public String fclub_code;
	public int id;
	public String img;
	public double lat;
	public double lng;
	public String phone;
	public String price;
	public String province;
	public String service;
	public String type;
	public String workTime;
}
