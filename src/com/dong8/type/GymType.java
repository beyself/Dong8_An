package com.dong8.type;

@SuppressWarnings("rawtypes")
public class GymType  implements Comparable
{
	public String typename;
	public int sort=10;
	public int count=0;//
	public String id;
	
	@Override
	public int compareTo(Object o) {
		GymType ss = (GymType) o;
		
		return (this.sort-ss.sort);
	}
}
