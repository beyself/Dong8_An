package com.dong8.sys;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;

import com.alibaba.fastjson.JSON;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.dong8.type.Gym;
import com.dong8.util.PreferencesUtils;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.MemoryCacheAware;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.tencent.android.tpush.XGPushManager;

public class MyApp extends Application {

	private static Context mContext;
	public Gym mCurrentGym = null;
	public Activity mFinishActivity;

	public static Context getContext() 
	{
		return mContext;
	}

	private List<Activity> activities = new ArrayList<Activity>();
	public static String netno = "";
	public static int icity = 0;

	public void addActivity(Activity activity) 
	{
		activities.add(activity);
	}

	public void removeActivity(Activity activity) 
	{
		activities.remove(activity);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mContext = getApplicationContext();
		initImageLoader(getApplicationContext());
		if (!Constants.DEBUG_MODE) {
			MyUncaughtExceptionHandler exceptionHandler = MyUncaughtExceptionHandler
					.getExceptionHandler(getApplicationContext());
			Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
		}
		
		String gym = PreferencesUtils.getString(this, "user_gym");
		if(gym != null && gym.length() > 0)
		{
			mCurrentGym = JSON.parseObject(gym, Gym.class);
		}
		
		XGPushManager.registerPush(getApplicationContext());
		SDKInitializer.initialize(this);
	}

	public static void initImageLoader(Context context) {
		int memoryCacheSize = (int) (Runtime.getRuntime().maxMemory() / 8);

		MemoryCacheAware<String, Bitmap> memoryCache;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			memoryCache = new LruMemoryCache(memoryCacheSize);
		} else {
			memoryCache = new LRULimitedMemoryCache(memoryCacheSize);
		}

		// This configuration tuning is custom. You can tune every option, you
		// may tune some of them,
		// or you can create default configuration by
		// ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.NORM_PRIORITY - 2).memoryCache(memoryCache)
				.denyCacheImageMultipleSizesInMemory().discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.FIFO) // LIFO
				// .enableLogging() // enable log
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

	public void exit() {

		for (Activity activity : activities) {
			activity.finish();
		}
		// System.exit(0);
	}

}
