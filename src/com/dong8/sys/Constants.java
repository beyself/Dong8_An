package com.dong8.sys;

public class Constants 
{
	public static final boolean	DEBUG_MODE					= true;
	static final public String APP_SERVER					= "http://182.92.79.94:8080/dong8/";
	static final public String GYM_SERVER					= "http://112.126.73.95:8080/Club/";
	
	static final public String CLUB_LIST					= APP_SERVER + "club/mlist";
	static final public String CLUB_DETAIL					= APP_SERVER + "club/getdetail";
	static final public String CLUB_PIC						= APP_SERVER + "club/getpic";
	static final public String CLUB_TYPE					= APP_SERVER + "club/movetype";
	static final public String CLUB_TYPE_COUNT				= APP_SERVER + "club/mtcount";
	static final public String CLUB_TYPE_GYM				= APP_SERVER + "club/slist";
	static final public String CLUB_DISTANCE				= APP_SERVER + "club/listbykey";
	static final public String CLUB_CITY					= APP_SERVER + "club/listbycity";
	static final public String USER_CHECK_TEL				= APP_SERVER + "user/registphone";
	static final public String USER_REGISTER				= APP_SERVER + "user/regist";
	static final public String USER_LOGIN					= APP_SERVER + "user/login";
	static final public String USER_SHOW					= APP_SERVER + "user/show";
	static final public String MEMBER_BAND					= APP_SERVER + "user/bindcard";
	static final public String USER_ORDER					= APP_SERVER + "user/getorder";
	static final public String USER_MEMBER					= APP_SERVER + "user/getuser";
	static final public String INIT_ORDER					= APP_SERVER + "user/initorder";
	static final public String CONFIRM_ORDER				= APP_SERVER + "user/submitorder";
	static final public String CITY_LIST					= APP_SERVER + "css/city.css";
	
	static final public String CLUB_GET_MEMBER 				="/member/getMember"; 
	static final public String CLUB_MEMBER_PAY 				="/member/buyTickets"; 
	
	//-------------
	static final public String TY_tennis="tennis";
	static final public String TY_tableTennis="tableTennis";
	static final public String TY_swimming="swimming";
	static final public String TY_squash="squash";
	static final public String TY_ski="ski";
	static final public String TY_skating="skating";
	static final public String TY_shooting="shooting";
	static final public String TY_roller="roller";
	static final public String TY_kart="kart";
	static final public String TY_golf="golf";
	static final public String TY_football="football";
	static final public String TY_equestrian="equestrian";
	static final public String TY_darts="darts";
	static final public String TY_car="car";
	static final public String TY_bowling="bowling";
	static final public String TY_billiards="billiards";
	static final public String TY_bicycle="bicycle";
	static final public String TY_basketball="basketball";
	static final public String TY_badminton="badminton";
	static final public String TY_archery="archery";
	
	
	//----------Preferences-----
	static final public String ID_Favorite_LIST="Favorite_LIST";
	static final public String ID_Favorite="FID-";
}
