package com.dong8.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dong8.R;
import com.dong8.resp.MyInfoResp.Member;

public class MemberAdapter extends BaseAdapter {

	private Context context;
	private List<Member> mList;
	@SuppressWarnings("unused")
	private int mCurrentPosition;

	public MemberAdapter(Context context, List<Member> mList) {
		this.context = context;
		this.mList = mList;
	}

	public void setCurrentPosition(int currentPosition) {
		mCurrentPosition = currentPosition;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (mList == null) {
			return 0;
		} else {

			return mList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = View.inflate(context, R.layout.item_member_card, null);
			holder.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
			holder.tvCard = (TextView) view.findViewById(R.id.tvCard);
			holder.tvMoney = (TextView) view.findViewById(R.id.tvMoney);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		Member temp = (Member) mList.get(position);
		holder.tvCard.setText(" 会员卡号: "+temp.member_card);
		holder.tvTitle.setText(temp.card_desc);
		holder.tvMoney.setText(" 当前余额: ￥ "+temp.money);
		
		return view;
	}

	public static class ViewHolder {
		public TextView tvTitle;
		public TextView tvCard;
		public TextView tvMoney;
	}
}
