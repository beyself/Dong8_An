package com.dong8.adapter;

import java.util.List;

import com.dong8.R;
import com.dong8.type.DetailGymType;
import com.dong8.util.TLog;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MovetypeAdapter extends BaseAdapter {

	private Context context;
	private List<DetailGymType> movetypeList;
	@SuppressWarnings("unused")
	private int mCurrentPosition;

	public MovetypeAdapter(Context context, List<DetailGymType> movetypeList) {
		this.context = context;
		this.movetypeList = movetypeList;
	}

	public void setCurrentPosition(int currentPosition) {
		mCurrentPosition = currentPosition;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (movetypeList == null) {
			return 0;
		} else {

			return movetypeList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return movetypeList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = View.inflate(context, R.layout.item_field, null);
			holder.price = (TextView) view.findViewById(R.id.price);
			holder.title = (TextView) view.findViewById(R.id.title);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		DetailGymType temp = (DetailGymType) movetypeList.get(position);
		TLog.i("hansen", "=======::" + temp.mtypeId);

		if (temp.mtypeId.equals("badminton")) {
			holder.title.setText("羽毛球");
		} else if (temp.mtypeId.equals("tableTennis")) {
			holder.title.setText("乒乓球");
		}

		holder.price.setText("会员价: ￥80\n非会员: ￥100");
		return view;
	}

	public static class ViewHolder {

		public TextView title;
		public TextView price;
	}
}
