package com.dong8.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dong8.R;
import com.dong8.sys.Constants;
import com.dong8.type.GymType;

public class TypeAdapter extends BaseAdapter {

	private Context context;
	List<GymType> typeList;
	@SuppressWarnings("unused")
	private int mCurrentPosition;

	public TypeAdapter(Context context, List<GymType> typeList) {
		this.context = context;
		this.typeList = typeList;
	}

	public void setCurrentPosition(int currentPosition) {
		mCurrentPosition = currentPosition;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (typeList == null) {
			return 0;
		} else {

			return typeList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return typeList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = View.inflate(context, R.layout.item_sport_type, null);
			holder.img = (ImageView) convertView.findViewById(R.id.img);
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.distance = (TextView) convertView.findViewById(R.id.distance);
//			holder.tvNum = (TextView) convertView.findViewById(R.id.tvNum);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		


		GymType type = typeList.get(position);

		holder.title.setText(type.typename);
	/*	if(type.num>0){
			holder.tvNum.setText(type.num+"家在线预定");
		}*/
		
		// {"id":"tennis","typeName":"网球"}

		if (type.id.equals(Constants.TY_archery)){
			holder.img.setBackgroundResource(R.drawable.ic_archery);
		}else if(type.id.equals(Constants.TY_badminton)){
			holder.img.setBackgroundResource(R.drawable.ic_badminton);
		}else if(type.id.equals(Constants.TY_basketball)){
			holder.img.setBackgroundResource(R.drawable.ic_basketball);
		}else if(type.id.equals(Constants.TY_bicycle)){
			holder.img.setBackgroundResource(R.drawable.ic_bicycle);
		}else if(type.id.equals(Constants.TY_billiards)){
			holder.img.setBackgroundResource(R.drawable.ic_billiards);
		}else if(type.id.equals(Constants.TY_bowling)){
			holder.img.setBackgroundResource(R.drawable.s8);
		}else if(type.id.equals(Constants.TY_car)){
			holder.img.setBackgroundResource(R.drawable.ic_car);
		}else if(type.id.equals(Constants.TY_darts)){
			holder.img.setBackgroundResource(R.drawable.ic_darts);
		}else if(type.id.equals(Constants.TY_equestrian)){
			holder.img.setBackgroundResource(R.drawable.ic_equestrian);
		}else if(type.id.equals(Constants.TY_football)){
			holder.img.setBackgroundResource(R.drawable.ic_football);
		}else if(type.id.equals(Constants.TY_golf)){
			holder.img.setBackgroundResource(R.drawable.ic_golf);
		}else if(type.id.equals(Constants.TY_kart)){//-----------有问题
			holder.img.setBackgroundResource(R.drawable.ic_car);
		}else if(type.id.equals(Constants.TY_roller)){
			holder.img.setBackgroundResource(R.drawable.ic_roller);
		}else if(type.id.equals(Constants.TY_shooting)){
			holder.img.setBackgroundResource(R.drawable.ic_shooting);
		}else if(type.id.equals(Constants.TY_skating)){
			holder.img.setBackgroundResource(R.drawable.ic_skating);
		}else if(type.id.equals(Constants.TY_ski)){
			holder.img.setBackgroundResource(R.drawable.ic_ski);
		}else if(type.id.equals(Constants.TY_squash)){//-----------有问题
			holder.img.setBackgroundResource(R.drawable.s8);
		}else if(type.id.equals(Constants.TY_swimming)){
			holder.img.setBackgroundResource(R.drawable.ic_swimming);
		}else if(type.id.equals(Constants.TY_tableTennis)){
			holder.img.setBackgroundResource(R.drawable.ic_tabletennis);
		}else if(type.id.equals(Constants.TY_tennis)){
			holder.img.setBackgroundResource(R.drawable.ic_tennis);
		}

		return convertView;
	}
	public final class ViewHolder {
		public TextView title;
		public TextView distance;
		public TextView tvNum;
		public ImageView img;
	}
}
