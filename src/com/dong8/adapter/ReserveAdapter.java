package com.dong8.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.dong8.R;
import com.dong8.activity.PayOrderActivity;
import com.dong8.resp.RespOrder.OrderForm;
import com.dong8.resp.SpaceSection;
import com.dong8.util.TLog;

@SuppressLint("SimpleDateFormat")
public class ReserveAdapter extends BaseAdapter implements OnClickListener
{
	private Context context;
	private List<OrderForm> orderList;
	@SuppressWarnings("unused")
	private int mCurrentPosition;

	public ReserveAdapter(Context context, List<OrderForm> orderList) {
		this.context = context;
		this.orderList = orderList;
	}

	public void setCurrentPosition(int currentPosition) {
		mCurrentPosition = currentPosition;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (orderList == null) {
			return 0;
		} else {

			return orderList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return orderList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = View.inflate(context, R.layout.item_reserve, null);
			holder.img = (ImageView) view.findViewById(R.id.img);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.price = (TextView) view.findViewById(R.id.price);
			holder.time = (TextView) view.findViewById(R.id.time);
			holder.cancel = (TextView) view.findViewById(R.id.cancel);
			holder.pay = (TextView) view.findViewById(R.id.pay);
			holder.left = (TextView) view.findViewById(R.id.left_time);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		OrderForm temp = (OrderForm) orderList.get(position);

		try
		{
			TLog.i("hansen", "=======::" + temp.amount);
			holder.price.setText("" + temp.amount + " 元(定金)");
			holder.title.setText(temp.order_desc + ": " + SpaceSection.getSingleSpaceInfo(temp.order_item)
					+ " (" + temp.club_name + ")");
			holder.time.setText(SpaceSection.getStartTime(temp.order_item));
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date date = sdf.parse(SpaceSection.getStartTime(temp.order_item));
			
			int sec = (int)(System.currentTimeMillis()/1000);
			int oldSec = (int)(date.getTime()/1000);
			int offset = oldSec - sec;
			
			if(temp.status.equals("3"))
			{
				holder.cancel.setVisibility(View.GONE);
				holder.pay.setVisibility(View.GONE);
				holder.left.setVisibility(View.VISIBLE);
				
				if(offset < 0)
				{
					holder.left.setText("已完成");
				}
				else
				{
					StringBuffer buffer = new StringBuffer();
					int hour = offset/3600;
					buffer.append(hour + "时");
		
					int min = (offset - hour*3600)/60;
					buffer.append(min + "分");

					sec = offset%60;
					buffer.append(sec + "秒");
					
					holder.left.setText(buffer.toString());
				}
			}
			else
			{
				if(offset < 0)
				{
					holder.cancel.setVisibility(View.GONE);
					holder.pay.setVisibility(View.GONE);
					holder.left.setVisibility(View.VISIBLE);
					
					holder.left.setText("已失效");
				}
				else
				{
					holder.cancel.setVisibility(View.VISIBLE);
					holder.pay.setVisibility(View.VISIBLE);
					holder.left.setVisibility(View.GONE);
					
					holder.cancel.setTag(String.valueOf(position));
					holder.pay.setTag(String.valueOf(position));
					
					holder.cancel.setOnClickListener(ReserveAdapter.this);
					holder.pay.setOnClickListener(ReserveAdapter.this);
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return view;
	}

	public static class ViewHolder 
	{
		public TextView title;
		public TextView time;
		public TextView price;
		public ImageView img;
		public TextView cancel;
		public TextView pay;
		public TextView left;
	}

	@Override
	public void onClick(View arg0) 
	{
		int pos = Integer.valueOf((String)arg0.getTag());
		if(arg0.getId() == R.id.cancel)
		{
			
		}
		else
		{
			Intent intent = new Intent(context, PayOrderActivity.class);
			intent.putExtra("orderForm", JSON.toJSONString(orderList.get(pos)));
			context.startActivity(intent);
		}
	}
}
