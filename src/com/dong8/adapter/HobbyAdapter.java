package com.dong8.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dong8.R;
import com.dong8.type.Gym;
import com.dong8.util.ImageLoaderHelper;

public class HobbyAdapter extends BaseAdapter {

	private Context context;
	private List<Gym> mList;
	@SuppressWarnings("unused")
	private int mCurrentPosition;

	public HobbyAdapter(Context context, List<Gym> mList) {
		this.context = context;
		this.mList = mList;
	}

	public void setCurrentPosition(int currentPosition) {
		mCurrentPosition = currentPosition;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (mList == null) {
			return 0;
		} else {

			return mList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			holder = new ViewHolder();
			view = View.inflate(context, R.layout.item_gym, null);
			holder.title = (TextView) view.findViewById(R.id.title);
			holder.distance = (TextView) view.findViewById(R.id.distance);
			holder.price = (TextView) view.findViewById(R.id.price);
			holder.img = (ImageView) view.findViewById(R.id.img);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		Gym temp = (Gym) mList.get(position);
		
		
		holder.title.setText(" "+temp.clubName);
		holder.price.setText(" " +temp.price);
		ImageLoaderHelper.displayImage(R.drawable.default_img_small, holder.img, temp.img);
		
		return view;
	}

	public static class ViewHolder {
		public TextView title;
		public TextView distance;
		public TextView price;
		public ImageView img;
	}
}
