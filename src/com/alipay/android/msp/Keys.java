﻿/* Copyright (C) 2010 The MobileSecurePay Project
 * All right reserved.
 * author: shiqun.shi@alipay.com
 * 
 *  提示：如何获取安全校验码和合作身份者id
 *  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
 *  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
 */

package com.alipay.android.msp;

//
// 请参考 Android平台安全支付服务(msp)应用开发接口(4.2 RSA算法签名)部分，并使用压缩包中的openssl RSA密钥生成工具，生成一套RSA公私钥。
// 这里签名时，只需要使用生成的RSA私钥。
// Note: 为安全起见，使用RSA私钥进行签名的操作过程，应该尽量放到商家服务器端去进行。
public final class Keys 
{
/*	//合作身份者id，以2088开头的16位纯数字
	public static final String DEFAULT_PARTNER = "2088202530580696";

	//收款支付宝账号
	public static final String DEFAULT_SELLER = "personalrpt@163.com";
	
	public static final String PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJdFTN5bfefR0aOs" +
										"dwVUItpSaOq5tI6jv26w+82NFliJ5mOaNhy5boHipNhlhpw1y5488AussopEpqJb" +
										"p/RxLVt7W7Vgu5hB9Uir88t8UL7JHti7Q+27L9MLVfLUWNbS05PZjNVNZLmyBdQF" +
										"rX7ZH7Fpto52G6z8nxa/3Hu/wcLRAgMBAAECgYAjeWv6ZUhUef8IvCQ5Sq7VtifG" +
										"jABMxxfYlIaogKKx5rzxtLNl7xJ8QTNZUA3OSktS84Vzp29DP7A4OPhdOcFh63H2" +
										"uG8gtfH6O3tA+w0W/b2q/L7KfttZuETaD5++Tuw7pjtyAZdzZg7iGPAjXMg78JOt" +
										"r1ddyAyZ6kt1KQNM1QJBAMY29sYJqNTtjJs0raalibLjIUANLMtBCH2V6MBVw+qn" +
										"CUEwNtdDX7quzSPtKpyHMDyKSIqmdYrM3bLsB/MA5YMCQQDDXtdRBiq05K9/m1ix" +
										"8EQlw2AGkbrL9NhAhOsjbENq2uV3rerZcqMl7LQIipJ8IcMbjmlIM+LamnkIDXYK" +
										"aNobAkEApZJUIKkA9pxXz235gDLXCy7fH3nh6Qax6PXILqF1vhU5jdKQfNAyruZF" +
										"RbVe2tGyYV/BqM/Tb9Yhesve+AXPsQJALsv+40eI3z+P0g8R1b1pdWoxDCHIDqad" +
										"TgcIk7mRBH1eTaZ5BJDzB1iFXy5iHB2yMF2SVlsnUZmCuC0RQzkOVQJAVfnnFyml" +
										"PVw4tWGLdkLibd7uwfPiXo4p4CNzzIoRkpe66LexoHarO4MxPKkF76Yc7RxVFbF3" +
										"2qk8de+L4QyZOg==";
										*/
	
	public static final String DEFAULT_PARTNER = "2088002412624869";

	//收款支付宝账号
	public static final String DEFAULT_SELLER = "lxerp@163.com";

	//商户私钥，自助生成
/*	public static final String PRIVATE = "MIICXAIBAAKBgQCXRUzeW3" +
			"3n0dGjrHcFVCLaUmjqubSOo79usPvNjRZYieZjmjYcuW6B4qTYZYacNcuePPALrLKKRKaiW6f0cS1be1u1YLuYQfVIq" +
			"/PLfFC+yR7Yu0Ptuy/TC1Xy1FjW0tOT2YzVTWS5sgXUBa1+2R+xabaOdhus/J8Wv9x7v8HC0QIDAQAB" +
			"AoGAI3lr+mVIVHn/CLwkOUqu1bYnxowATMcX2JSGqICisea88bSzZe8SfEEzWVAN" + 
			"zkpLUvOFc6dvQz+wODj4XTnBYetx9rhvILXx+jt7QPsNFv29qvy+yn7bWbhE2g+f" +
			"vk7sO6Y7cgGXc2YO4hjwI1zIO/CTra9XXcgMmepLdSkDTNUCQQDGNvbGCajU7Yyb" + 
			"NK2mpYmy4yFADSzLQQh9lejAVcPqpwlBMDbXQ1+6rs0j7SqchzA8ikiKpnWKzN2y" +
			"7AfzAOWDAkEAw17XUQYqtOSvf5tYsfBEJcNgBpG6y/TYQITrI2xDatrld63q2XKj" +
			"Jey0CIqSfCHDG45pSDPi2pp5CA12CmjaGwJBAKWSVCCpAPacV89t+YAy1wsu3x95" + 
			"4ekGsej1yC6hdb4VOY3SkHzQMq7mRUW1XtrRsmFfwajP02/WIXrL3vgFz7ECQC7L" +
			"/uNHiN8/j9IPEdW9aXVqMQwhyA6mnU4HCJO5kQR9Xk2meQSQ8wdYhV8uYhwdsjBd" +
			"klZbJ1GZgrgtEUM5DlUCQFX55xcppT1cOLVhi3ZC4m3e7sHz4l6OKeAjc8yKEZKX" +
			"uui3saB2qzuDMTypBe+mHO0cVRWxd9qpPHXvi+EMmTo=";*/
	
	public static final String PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANk33xJcY4HrpLvH" +
										"U80tRYGNrFgu2WUzZkk6+KFj6F4ic193lwQxbkw6DlagzhyVF+GvpxMus4xPHq3T" +
										"A4Vbiz3ka0t8Ig3DjU/Zq6rQNlpIEvEcr+/EseSLm690Z+Gu1yriYaI+7Czm8YJr" +
										"JV6Qgl3KSe94GedVMfGkBI7JOueTAgMBAAECgYANFdyOUpp7/SITXdKijZOeVlaH" +
										"iWr3Ob1zs+W+czJQWWrx2VZznjayyNyMQ2unNSQXKGm8MhgG381NU2Tm57jfiTWF" +
										"8EAEcbZRURZ1oWQzRnXFn5UikvSX2wpDykEO9/6eYXzATV37bCeLBsx286yWhHdA" +
										"4MXECfBi6vaDVwbr4QJBAO2wQJVIjEDCHKUQwhxXQVbi7hrqX/MVDqGTqS1AHdk9" +
										"eaoWuxlCOuoX1eQk+TQsg3zcvkJgqYNRON24r0emNI0CQQDp8+Z5hvsLTboeDeTS" +
										"W8/i2qxJ9SV4cVaWHeM6poS9HdFgCGM/tlgKqgCr3YbPhrtebiR/u4XxwMNzdseL" +
										"SlSfAkBT0Y7E1iOXhpxhXUqGggKpqDb9SN9zMl7Jfe/v5Cdif3pTD9cE7kUmXidw" +
										"X9V9n6DyvElf0QSWKWNbVRCCvMrVAkEAnz6OVZBEVyhcFFL+uxUVhuOzvE+qZrY2" +
										"6s2DOwvXdGbUepqa++YhPXkEJfgO6DlcVmWAYJWDEdBU+cYTA0PgHwJBAKJi6hsx" +
										"locuipQ3nz9x8rEqeFvJcEiZN6ccyhaB5bSV5XRvOhO74FbQDgZLrboxNJIKVh6A" +
										"7SJmkMXdH0dQG8k=";

	public static final String PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
}
